<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dimension');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.GP+L%g$B+0p*DoK=W#ozB[:*n=*U7WMa,TCu[pH`n4D?uk/65XRKMJ{Jl@>vgWd');
define('SECURE_AUTH_KEY',  't>~EzK_2Zs>}AUj?)(+%>2-Mg8^dSa[45W`J?--hmE?!O69hFP9{ F.9*DiJPVxE');
define('LOGGED_IN_KEY',    'O.TXaU{evXC~3SHa?=j%M5l^,;PJ/VQUwRLA+eN.od@NB+bJntOWrKJ$z)Xp<i,*');
define('NONCE_KEY',        'yJoevD*Ta}m4]z~2?EgO0-^;-d&QU@bPsp>(&,D#7trC0%a@Z)i{%9G$:5=kXM6h');
define('AUTH_SALT',        '|whmQvXUkB9Jc}YT&URAN$Z]_/QcaB=TQ2h|bZ7(H|.ec-Y3=h`{nQY OM7|)~v!');
define('SECURE_AUTH_SALT', '+Cr;)A[l2b% -0y&Y6o335)U=Z((}##Yv_+wAfT|@yr[2o|/oz##eo5usKRYl)~v');
define('LOGGED_IN_SALT',   'e:WO#e~srfA^DJ,M&XEC]@0#rTG<<u:{8L*KjF;QhI!9qY4N^oSZySNHl4!%uY|T');
define('NONCE_SALT',       '<F_$Fhi-4ZA_}&i.Q0/uRIm.o3NAv@[yS2`Fi6D w6BoJ9tYz)nA#ZxFqwA|?er-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
