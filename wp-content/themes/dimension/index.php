<?php
    /*
    Template Name: Home
    */
get_header() ?>
<body id="top" class="page-template page-template-page-builder page-template-page-builder-php page page-id-2606 wpb-js-composer js-comp-ver-5.4.7 vc_responsive">
      <div class="al-preloader-fixed" id="preloader">
         <div style="background-color: #555" id="status"></div>
      </div>

       <header id="top-nav" class="top-nav page-header al-have-full-screen">
   <div class="container">
      <a href="http://kanter.fidex.com.ua/" class="logo smooth-scroll"> <img src="<?php the_field('logo'); ?>" alt=""></a>
      <nav class="top-menu">
         <ul id="menu-top-nav" class="sf-menu sf-js-enabled sf-arrows" style="touch-action: pan-y;">
                  <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-184"><a href="#home">Home</a></li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-186"><a href="#about">About</a></li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2927"><a href="#systems">Systems</a></li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2927"><a href="#careers">Careers</a></li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2927"><a href="#contact">Contact Us</a></li>
         </ul>
      </nav>
      <a href="#" class="toggle-mnu"><span></span></a>
      <div id="mobile-menu">
         <div class="inner-wrap">
            <nav>
               <ul id="menu-top-nav-1" class="nav_menu">
                  <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-184"><a href="#home">Home</a></li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-186"><a href="#about">About</a></li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2927"><a href="#systems">Systems</a></li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2927"><a href="#careers">Careers</a></li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2927"><a href="#contact">Contact Us</a></li>
               </ul>
            </nav>
         </div>
      </div>
   </div>
</header> 
      <div class="top icon-down toTopFromBottom"><a href="#" class="al-btn-to-top"><i class="pe-7s-angle-up"></i></a></div>
      <div id="home" class="al-side-container">
         <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
            <div class="wpb_column vc_column_container vc_col-sm-12">
               <div class="vc_column-inner ">
                  <div class="wpb_wrapper">
                     <link href="http://fonts.googleapis.com/css?family=Lato:400%2C500%7CPlayfair+Display:400" rel="stylesheet" property="stylesheet" type="text/css" media="all">
                     <div id="rev_slider_8_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-source="gallery" style="background:#1f1d24;padding:0px; height: 100vh;">
                        <div id="rev_slider_8_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.7.3">
                           <ul>
                              <li data-index="rs-22" data-transition="fadethroughdark" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="2000"  data-thumb=""  data-rotate="0"  data-saveperformance="off"  data-title="Dimension" data-param1="01" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                 <img src="<?php echo get_template_directory_uri(); ?>/images/DSC_0089.jpg"  alt="" title="Home &#8211; Demo 2"  data-bgposition="center center" data-bgfit="cover" data-bgparallax="3" class="rev-slidebg" data-no-retina>
                                 <div class="rs-background-video-layer" 
                                    data-forcerewind="on" 
                                    data-volume="mute" 
                                    data-videowidth="100%" 
                                    data-videoheight="100%" 
                                    data-videomp4="http://dimension.blueinspires.com/video/DIMENSION-ALL.mp4" 
                                    data-videopreload="auto" 
                                    data-videoloop="loop" 
                                    data-aspectratio="16:9" 
                                    data-autoplay="true" 
                                    data-autoplayonlyfirsttime="false" 
                                    ></div>
                                 <div class="tp-caption Creative-Button rev-btn  rs-parallaxlevel-15" 
                                    id="slide-22-layer-6" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['top','top','top','top']" data-voffset="['694','611','689','540']" 
                                    data-fontweight="['400','500','500','500']"
                                    data-width="none"
                                    data-height="none"
                                    data-whitespace="nowrap" 
                                    data-type="button" 
                                    data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]'
                                    data-responsive_offset="on" 
                                    data-responsive="off"
                                    data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":3850,"ease":"Power2.easeOut"},{"delay":"wait","speed":500,"to":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;","ease":"Power1.easeIn"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(15,221,161,1);bc:rgba(15,221,161,1);bw:1px 1px 1px 1px;"}]'
                                    data-textAlign="['left','left','left','left']"
                                    data-paddingtop="[15,15,15,15]"
                                    data-paddingright="[50,50,50,50]"
                                    data-paddingbottom="[15,15,15,15]"
                                    data-paddingleft="[50,50,50,50]"  style="color: #000!important; background-color: rgba(255,255,255,0.5)!important; z-index: 10; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">CONTINUE THE JOURNEY</div>
                              </li>
                              <li data-index="rs-24" data-transition="fadethroughdark" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="2000"  data-thumb="http://kanter.fidex.com.ua/wp-content/uploads/revslider/creativefreedom/icebg-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Corporate" data-param1="02" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                 <img src="<?php echo get_template_directory_uri(); ?>/images/DSC_0094.jpg"  alt="" title="Home &#8211; Demo 2"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="3" class="rev-slidebg" data-no-retina>
                                 
                                 
                                 <div class="tp-caption Creative-SubTitle   tp-resizeme rs-parallaxlevel-2" 
                                    id="slide-23-layer-3" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-91','-91','-81','-64']" 
                                    data-fontsize="['14','14','14','12']"
                                    data-lineheight="['14','14','14','12']"
                                    data-width="none"
                                    data-height="none"
                                    data-whitespace="nowrap" 
                                    data-type="text" 
                                    data-responsive_offset="on"  data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":2350,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","ease":"Power3.easeInOut"}]'
                                    data-textAlign="['center','center','center','center']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="z-index: 13; white-space: nowrap;text-transform:left; color: #000!important; background-color: rgba(255,255,255, .6);"><span style="margin-top: -20px; padding: 5px 10px!important; display: block;">Shaping the Future of the Country's</span></div>
                                 <div class="tp-caption Creative-Title   tp-resizeme rs-parallaxlevel-1" 
                                    id="slide-24-layer-2" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-10','-10','-10','-10']" 
                                    data-fontsize="['70','70','50','40']"
                                    data-lineheight="['70','70','55','45']"
                                    data-width="['none','none','none','320']"
                                    data-height="none"
                                    data-whitespace="nowrap" 
                                    data-type="text" 
                                    data-responsive_offset="on"  data-frames='[{"from":"y:1000px;opacity:0;","speed":1500,"to":"o:1;","delay":2550,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","ease":"Power3.easeInOut"}]'
                                    data-textAlign="['center','center','center','center']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="color: #000!important; z-index: 20; white-space: nowrap;text-transform:left;">Concrete <span style="font-size: 30px; display: block; line-height:1;">&amp;</span>  Infrastructure</div>
                                 
                                 <div class="tp-caption Creative-Button rev-btn  rs-parallaxlevel-15" 
                                    id="slide-23-layer-6" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['top','top','top','top']" data-voffset="['694','611','689','540']" 
                                    data-fontweight="['400','500','500','500']"
                                    data-width="none"
                                    data-height="none"
                                    data-whitespace="nowrap" 
                                    data-type="button" 
                                    data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]'
                                    data-responsive_offset="on" 
                                    data-responsive="off"
                                    data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":3850,"ease":"Power2.easeOut"},{"delay":"wait","speed":500,"to":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;","ease":"Power1.easeIn"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(15,221,161,1);bc:rgba(15,221,161,1);bw:1px 1px 1px 1px;"}]'
                                    data-textAlign="['left','left','left','left']"
                                    data-paddingtop="[15,15,15,15]"
                                    data-paddingright="[50,50,50,50]"
                                    data-paddingbottom="[15,15,15,15]"
                                    data-paddingleft="[50,50,50,50]"    style="color: #000!important; background-color: rgba(255,255,255,0.5)!important; z-index: 10; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">CONTINUE THE JOURNEY</div>
                              </li>
                              <li data-index="rs-25" data-transition="fadethroughdark" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="2000"  data-thumb="http://kanter.fidex.com.ua/wp-content/uploads/revslider/creativefreedom/icebg-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Concept" data-param1="03" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                 <img src="<?php echo get_template_directory_uri(); ?>/images/DSC_0057-3.jpg"  alt="" title="Home &#8211; Demo 2"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="3" class="rev-slidebg" data-no-retina>
                                 
                                 
                                 <div class="tp-caption Creative-SubTitle   tp-resizeme rs-parallaxlevel-2" 
                                    id="slide-23-layer-3" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-91','-91','-81','-64']" 
                                    data-fontsize="['14','14','14','12']"
                                    data-lineheight="['14','14','14','12']"
                                    data-width="none"
                                    data-height="none"
                                    data-whitespace="nowrap" 
                                    data-type="text" 
                                    data-responsive_offset="on"  data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":2350,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","ease":"Power3.easeInOut"}]'
                                    data-textAlign="['center','center','center','center']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="z-index: 13; white-space: nowrap;text-transform:left; color: #000!important; background-color: rgba(255,255,255, .6);"><span style="margin-top: -20px; padding: 5px 10px!important; display: block;">Shaping the Future of the Country's</span></div>
                                 <div class="tp-caption Creative-Title   tp-resizeme rs-parallaxlevel-1" 
                                    id="slide-24-layer-2" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-10','-10','-10','-10']" 
                                    data-fontsize="['70','70','50','40']"
                                    data-lineheight="['70','70','55','45']"
                                    data-width="['none','none','none','320']"
                                    data-height="none"
                                    data-whitespace="nowrap" 
                                    data-type="text" 
                                    data-responsive_offset="on"  data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":2550,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","ease":"Power3.easeInOut"}]'
                                    data-textAlign="['center','center','center','center']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="color: #000!important; z-index: 20; white-space: nowrap;text-transform:left;">Concrete <span style="font-size: 30px; display: block; line-height:1;">&amp;</span>  Infrastructure</div>
                                 
                                 <div class="tp-caption Creative-Button rev-btn  rs-parallaxlevel-15" 
                                    id="slide-24-layer-6" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['top','top','top','top']" data-voffset="['694','611','689','540']" 
                                    data-fontweight="['400','500','500','500']"
                                    data-width="none"
                                    data-height="none"
                                    data-whitespace="nowrap" 
                                    data-type="button" 
                                    data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]'
                                    data-responsive_offset="on" 
                                    data-responsive="off"
                                    data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":3850,"ease":"Power2.easeOut"},{"delay":"wait","speed":500,"to":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;","ease":"Power1.easeIn"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(205,176,131,1);bc:rgba(205,176,131,1);bw:1px 1px 1px 1px;"}]'
                                    data-textAlign="['left','left','left','left']"
                                    data-paddingtop="[15,15,15,15]"
                                    data-paddingright="[50,50,50,50]"
                                    data-paddingbottom="[15,15,15,15]"
                                    data-paddingleft="[50,50,50,50]" style="color: #000!important; background-color: rgba(255,255,255,0.5)!important; z-index: 10; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">CONTINUE THE JOURNEY</div>
                              </li>
                           </ul>
                           <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
                              if(htmlDiv) {
                              	htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                              }else{
                              	var htmlDiv = document.createElement("div");
                              	htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                              	document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                              }
                           </script> 
                           <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                        </div>
                        <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss=".tp-caption.Creative-SubTitle,.Creative-SubTitle{color:rgba(205,176,131,1.00);font-size:14px;line-height:14px;font-weight:400;font-style:normal;font-family:Lato;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0 0 0 0px;letter-spacing:2px}.tp-caption.Creative-Title,.Creative-Title{color:rgba(255,255,255,1.00);font-size:70px;line-height:70px;font-weight:400;font-style:normal;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0 0 0 0px}.tp-caption.Creative-Button,.Creative-Button{color:rgba(15,221,161,1.00);font-size:13px;line-height:13px;font-weight:400;font-style:normal;font-family:Lato;text-decoration:none;background-color:rgba(0,0,0,0);border-color:rgba(205,176,131,0.25);border-style:solid;border-width:1px;border-radius:0px 0px 0px 0px;letter-spacing:2px}.tp-caption.Creative-Button:hover,.Creative-Button:hover{color:rgba(15,221,161,1.00);text-decoration:none;background-color:rgba(0,0,0,0);border-color:rgba(15,221,161,1.00);border-style:solid;border-width:1px;border-radius:0px 0px 0px 0px;cursor:pointer}";
                           if(htmlDiv) {
                           	htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                           }else{
                           	var htmlDiv = document.createElement("div");
                           	htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                           	document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                           }
                        </script> <script type="text/javascript">if (setREVStartSize!==undefined) setREVStartSize(
                           {c: '#rev_slider_8_1', responsiveLevels: [1240,1024,778,480], gridwidth: [1240,1024,778,480], gridheight: [868,768,960,720], sliderLayout: 'fullscreen', fullScreenAutoWidth:'off', fullScreenAlignForce:'off', fullScreenOffsetContainer:'', fullScreenOffset:'60px'});
                           		
                           var revapi8,
                           tpj;	
                           (function() {			
                           if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded",onLoad); else onLoad();	
                           function onLoad() {				
                           	if (tpj===undefined) { tpj = jQuery; if("off" == "on") tpj.noConflict();}
                           if(tpj("#rev_slider_8_1").revolution == undefined){
                           	revslider_showDoubleJqueryError("#rev_slider_8_1");
                           }else{
                           	revapi8 = tpj("#rev_slider_8_1").show().revolution({
                           		sliderType:"standard",
                           		jsFileLocation:"http://kanter.fidex.com.ua/wp-content/plugins/rev-slider/public/assets/js/",
                           		sliderLayout:"fullscreen",
                           		dottedOverlay:"none",
                           		delay:9000,
                           		navigation: {
                           			keyboardNavigation:"off",
                           			keyboard_direction: "horizontal",
                           			mouseScrollNavigation:"off",
                           							mouseScrollReverse:"default",
                           			onHoverStop:"off",
                           			touch:{
                           				touchenabled:"on",
                           				touchOnDesktop:"off",
                           				swipe_threshold: 75,
                           				swipe_min_touches: 50,
                           				swipe_direction: "horizontal",
                           				drag_block_vertical: false
                           			}
                           			,
                           			tabs: {
                           				style:"metis",
                           				enable:true,
                           				width:350,
                           				height:40,
                           				min_width:249,
                           				wrapper_padding:0,
                           				wrapper_color:"",
                           				tmp:'<div class="tp-tab-wrapper"><div class="tp-tab-number">{{param1}}</div><div class="tp-tab-divider"></div><div class="tp-tab-title-mask"><div class="tp-tab-title">{{title}}</div></div></div>',
                           				visibleAmount: 5,
                           				hide_onmobile: true,
                           				hide_under:800,
                           				hide_onleave:false,
                           				hide_delay:200,
                           				direction:"vertical",
                           				span:true,
                           				position:"inner",
                           				space:0,
                           				h_align:"left",
                           				v_align:"center",
                           				h_offset:0,
                           				v_offset:0
                           			}
                           		},
                           		responsiveLevels:[1240,1024,778,480],
                           		visibilityLevels:[1240,1024,778,480],
                           		gridwidth:[1240,1024,778,480],
                           		gridheight:[868,768,960,720],
                           		lazyType:"none",
                           		parallax: {
                           			type:"3D",
                           			origo:"slidercenter",
                           			speed:1000,
                           			speedbg:0,
                           			speedls:0,
                           			levels:[2,4,6,8,10,12,14,16,45,50,47,48,49,50,0,50],
                           			ddd_shadow:"off",
                           			ddd_bgfreeze:"on",
                           			ddd_overflow:"hidden",
                           			ddd_layer_overflow:"visible",
                           			ddd_z_correction:100,
                           		},
                           		spinner:"off",
                           		stopLoop:"on",
                           		stopAfterLoops:0,
                           		stopAtSlide:1,
                           		shuffle:"off",
                           		autoHeight:"off",
                           		fullScreenAutoWidth:"off",
                           		fullScreenAlignForce:"off",
                           		fullScreenOffsetContainer: "",
                           		fullScreenOffset: "60px",
                           		disableProgressBar:"on",
                           		hideThumbsOnMobile:"off",
                           		hideSliderAtLimit:0,
                           		hideCaptionAtLimit:0,
                           		hideAllCaptionAtLilmit:0,
                           		debugMode:false,
                           		fallbacks: {
                           			simplifyAll:"off",
                           			nextSlideOnWindowFocus:"off",
                           			disableFocusListener:false,
                           		}
                           	});
                           }; /* END OF revapi call */
                           
                           }; /* END OF ON LOAD FUNCTION */
                           }()); /* END OF WRAPPING FUNCTION */
                        </script> <script>var htmlDivCss = unescape("%23rev_slider_8_1%20.metis%20.tp-tab-number%20%7B%0A%20%20%20%20padding-left%3A5px%3B%0A%20%20%20%20color%3Argb%280%2C%200%2C%200%29%3B%0A%20%20%20%20font-size%3A40px%3B%0A%20%20%20%20line-height%3A42px%3B%0A%20%20%20%20font-weight%3A400%3B%0A%20%20%20%20Display%22%3B%0A%20%20%20%20width%3A%2080px%3B%20%20%20%0A%20%20%20%20display%3A%20inline-block%3B%0A%09position%3Aabsolute%3B%0A%20%20%20%20text-align%3Acenter%3B%0A%20%20%20%20box-sizing%3Aborder-box%3B%0A%7D%0A%0A%0A%23rev_slider_8_1%20.metis%20.tp-tab-mask%20%7B%0A%20%20%20left%3A0px%3B%0A%20%20%20top%3A0px%3B%0A%20%20%20max-width%3A80%20%21important%3B%20%20%20%0A%20%20%20line-height%3A45px%3B%0A%20%20%20transition%3A0.4s%20padding-left%2C%200.4s%20left%2C%200.4s%20max-width%3B%0A%7D%0A%0A%23rev_slider_8_1%20.metis%3Ahover%20.tp-tab-mask%7B%0A%20%20%20left%3A15px%3B%0A%20%20%20padding-left%3A0px%3B%0A%20%20%20max-width%3A500px%20%21important%3B%0A%7D%0A%0A%23rev_slider_8_1%20.metis%20.tp-tab-divider%20%7B%20%0A%09border-right%3A%201px%20solid%20transparent%3B%0A%20%20%20%20height%3A%2030px%3B%0A%20%20%20%20width%3A%201px%3B%0A%20%20%20%20display%3A%20inline-block%3B%0A%20%20%20%20position%3Aabsolute%3B%0A%20%20%20%20left%3A80px%3B%0A%20%20%20%20transition%3A0.4s%20all%3B%0A%7D%0A%0A%23rev_slider_8_1%20.metis%20.tp-tab-title%20%7B%0A%20%20%20%20color%3Argb%280%2C%200%2C%200%29%3B%0A%20%20%20%20font-size%3A28px%3B%0A%20%20%20%20line-height%3A42px%3B%0A%20%20%20%20font-weight%3A400%3B%0A%20%20%20%20Display%22%3B%0A%20%20%20%20position%3Arelative%3B%0A%20%20%20%20line-height%3A42px%3B%0A%20%20%20%20padding-left%3A%2030px%3B%0A%20%20%20%20display%3A%20inline-block%3B%0A%20%20%20%20transform%3Atranslatex%28-100%25%29%3B%0A%20%20%20%20transition%3A0.4s%20all%3B%0A%7D%0A%0A%23rev_slider_8_1%20.metis%20.tp-tab-title-mask%20%7B%0A%20%20%20position%3Aabsolute%3B%0A%20%20%20overflow%3Ahidden%3B%0A%20%20%20left%3A80px%3B%20%0A%7D%0A%0A%23rev_slider_8_1%20.metis%3Ahover%20.tp-tab-title%20%7B%0A%20%20%20transform%3Atranslatex%280%29%3B%0A%20%7D%0A%0A%23rev_slider_8_1%20.metis%20.tp-tab%20%7B%20%0A%09opacity%3A%200.15%3B%0A%20%20%20%20transition%3A0.4s%20all%3B%0A%7D%0A%0A%23rev_slider_8_1%20.metis%20.tp-tab%3Ahover%2C%0A%23rev_slider_8_1%20.metis%20.tp-tab.selected%20%7B%0A%20%20%20%20opacity%3A%201%3B%20%0A%7D%0A%0A%23rev_slider_8_1%20.metis%20.tp-tab.selected%20.tp-tab-divider%20%7B%0A%09border-right%3A%201px%20solid%20%23cdb083%3B%0A%7D%0A%0A%23rev_slider_8_1%20.metis%3Ahover%20.tp-tab-divider%20%7B%0A%09margin-left%3A15px%3B%0A%7D%0A%0A%23rev_slider_8_1%20.metis.tp-tabs%20%7B%0A%20%20%20max-width%3A80px%20%21important%3B%20%20%0A%7D%0A%20%20%0A%23rev_slider_8_1%20.metis.tp-tabs%3Abefore%20%7B%0A%20content%3A%22%20%22%3B%0A%20height%3A100%25%3B%0A%20width%3A80px%3B%20%0A%20border-right%3A1px%20solid%20rgba%28255%2C255%2C255%2C0.10%29%3B%0A%20left%3A0px%3B%0A%20top%3A0px%3B%0A%20position%3Aabsolute%3B%0A%20transition%3A0.4s%20all%3B%0A%20background%3Argba%280%2C0%2C0%2C0.15%29%3B%0A%20box-sizing%3Acontent-box%20%21important%3B%0A%20padding%3A0px%3B%0A%20%7D%0A%20%0A%20%23rev_slider_8_1%20.metis.tp-tabs%3Ahover%3Abefore%7B%0A%20%20width%3A80px%3B%0A%20%20background%3Argba%280%2C0%2C0%2C0.25%29%3B%0A%20%20padding%3A0px%2015px%3B%0A%20%7D%0A%20%20%20%20%20%0A%20%40media%20%28max-width%3A499px%29%7B%0A%20%23rev_slider_8_1%20.metis.tp-tabs%3Abefore%20%7B%0A%20background%3Argba%280%2C0%2C0%2C0.75%29%3B%0A%20%7D%0A%20%7D%0A%20%0A");
                           var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                           if(htmlDiv) {
                           	htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                           }
                           else{
                           	var htmlDiv = document.createElement('div');
                           	htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                           	document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                           }
                        </script> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
       
         <div class="vc_row-full-width vc_clearfix"></div>
         <section id="about" style="background-image: url(<?php the_field('about_background'); ?>" data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section vc_custom_1525183886901 vc_section-has-fill">
            <div class="vc_row wpb_row vc_row-fluid">
               <div class="wpb_column vc_column_container vc_col-sm-2">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper"></div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-8">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div style="" class="heading-title  al-center" id="alian4x_el_5b4d7e70f0d58">

                           <h2 style="" class="al-headitg-title al-parallax"><?php the_field('about_title'); ?></h2>
                           <div  class="al-small-description"><?php the_field('about_subtitle'); ?></div>
                           <div class="vc_row wpb_row vc_row-fluid">
                              <div class="vc_col-md-5">
                                 <?php the_field('about_content_left'); ?>
                              </div>
                              <div class="vc_col-md-5">
                                 <?php the_field('about_content_right'); ?>
                              </div>
                              <div class="vc_col-md-2"></div>
                           </div>
                           <!-- <h2 style="" class="al-headitg-title al-parallax">About us</h2>
                           <div  class="al-small-description">We create awesome stuff</div>
                           <div class="vc_row wpb_row vc_row-fluid">
                              <div class="vc_col-md-5">
                                 <p>Dimension-all began in 1976 as the first supplier of system formworks and scaffoldings to the Philippine market. The company carried Coffral and Staflex equipment and began by supplying such notable projects as the La Mesa and Magat Dams and the Metro Plaza Makati buildings.</p>
                                 <p>The 80's saw the company supplying Alimall Cubao, SM North Edsa, ADB Ortigas, GSIS (main) , and Purefoods Plants Baung Batangas projects. Through the 90's, we continued our relationships with contractors such as MDC, DDTKI, CE Construction, EEI Corp., Monolith Construction DATEM and HILMARC’S </p>
                              </div>
                              <div class="vc_col-md-5">
                                 <p>In 2003, we began our partnership with Aluma Systems of Canada and are now their sole distributor for the Philippines. Systems has a commitment to innovation and excellence that is seen in their Table and Truss offerings for vertical construction and their Aluma Frame system for heavy infrastructure.</p>
                                 <p>In August 2015, Dimension-all Inc. became a subsidiary and the sixth Asian member of the SRG Group of Companies, a Japan based formworks and shoring company listed in the Japan Stock Exchange, with factories and offices based in Japan, Vietnam, Korea , Myanmar ,Thailand and the Philippines.</p>
                              </div>
                              <div class="vc_col-md-2"></div>
                           </div> -->
                        </div>
                     </div>
                  </div>
               </div>
               
            </div>

            <div class="yellow-bg logo-partner">
                  <ul>
                    <?php
                     $sponsor_image = get_field('field_5b78d37879e94');
                     foreach ($sponsor_image as $image_item) { ?>
                        <li><img src="<?php echo $image_item['about_sponsor_image']; ?>" alt=""></li>
                     <?php }
                    ?>
                  </ul>
               </div>
         </section>
        <div class="vc_row-full-width vc_clearfix"></div>
    <div style="opacity: 1; padding-bottom: 0!important;" id="systems" data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_custom_1525183328188 vc_row-has-fill vc_row-no-padding">
            <div class="wpb_column vc_column_container vc_col-sm-12">
               <div class="vc_column-inner vc_custom_1520177605244">
                  <div class="wpb_wrapper">
                     <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1520177600677">
                        <div class="wpb_column vc_column_container vc_col-sm-4">
                           <div class="vc_column-inner ">
                              <div class="wpb_wrapper">
                                 <div style="" class="heading-title  al-center" id="alian4x_el_5b4d7e7111623">
                                    <h2 style="" class="al-headitg-title al-parallax"><?php the_field('works_title'); ?></h2>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-1">
                           <div class="vc_column-inner ">
                              <div class="wpb_wrapper">
                                 
                              </div>
                           </div>
                        </div>

                        <div class="wpb_column vc_column_container vc_col-sm-7">
                           <div class="vc_column-inner ">
                              <div class="wpb_wrapper">
                                 <div class="al-controls-portfolio center">
                                 <ul id="al-control-portfolio" class="text-center">
                                    <li id="liact" class="filter select-cat active" data-filter="*">All</li>
                                    <?php 
                                       $portfolio_list = get_field('field_5b78df5a37eea');
                                       foreach($portfolio_list as $portfolio_item) {
                                        ?>
                                          <li id="liact" class="filter select-cat" data-filter=".<?php echo str_replace(' ', '', $portfolio_item['works_list_title_text']); ?>">
                                             <?php echo $portfolio_item['works_list_title_text']; ?>
                                          </li>
                                    <?php }
                                    ?>
                                 </ul>
                              </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     
                     <div style="" class="al-portfolio-layout">
                        <div id="alian4x_el_5b4d7e7111938" data-size="3" class="al-posts al-masonry-posts">
                           <div class="gutter-sizer"></div>
                           
                           <?php 
                           $portfolio_image = get_field('field_5b78dfbc37eec');
                           foreach($portfolio_image as $portfolio_item) {
                           ?>
                           <article class="<?php echo str_replace(' ', '', $portfolio_item['work_portfolio_slug']); ?>">
							   <a href="/portfolio/#<?php echo $portfolio_item['work_portfolio_slug']; ?>">
                                   <div class="item-wrap dh-container">
                                      <div class="post-thumb">
                                          <img width="600" height="450" src="<?php echo $portfolio_item['work_portfolio_image']; ?>" class="attachment-kanter_portfolio_popup size-kanter_portfolio_popup wp-post-image" alt="" /></div>
                                      <div class="content dh-overlay">
                                       
                                         <div class="content-wrap">
                                            <div class="content-va">
                                               <h2><?php echo $portfolio_item['work_portfolio_title']; ?> <br><span style="font-size: 14px;">Specification</span></h2>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                                 </a>
                           </article>
                           <?php
                              }
                           ?>

                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>     
     
         
 <div class="vc_row-full-width vc_clearfix"></div>
         <section style="opacity: 1; padding: 0;" data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true" class="wpb_row vc_row-fluid vc_row-no-padding">
        <div class="yellow-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <p><?php the_field('work_quote'); ?></p>
                </div>
                <div class="col-md-3">
                     <a href="#contact" class="btn btn-default">Get a quote</a>
                </div>
            </div>
        </div>
        </div>
         </section>   
  <div class="vc_row-full-width vc_clearfix"></div>
         <section id="careers" style="opacity: 1; padding-top: 0!important;" data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true" class="wpb_row vc_row-fluid vc_row-no-padding">
            <div class="tag-container" style="background-image: url(<?php echo the_field('team_bg'); ?>)">
                <div class="container">
                     <div class="row">
                        <div class="col-md-8">
                            <h1 style="color: #000;"><?php echo the_field('team_title'); ?></h1>
                            <p style="color: #212121;"><?php echo the_field('team_text'); ?></p>
                            
                            <a href="#" class="btn btn-default" style="margin: 1em 0 2em;">Join Our Team</a>
                            <?php echo do_shortcode('[contact-form-7 id="10" title="Contact form 1"]'); ?>
                        </div>
                    </div>
                </div>
            </div>
         </section>
           <div class="vc_row-full-width vc_clearfix"></div>
         <div style="opacity: 1; padding-bottom: 0!important;" id="site-photo" data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_custom_1525183328188 vc_row-has-fill vc_row-no-padding">
            <div class="wpb_column vc_column_container vc_col-sm-12">
               <div class="vc_column-inner vc_custom_1520177605244">
                  <div class="wpb_wrapper">
                     <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1520177600677">
                        <div class="wpb_column vc_column_container vc_col-sm-2">
                           <div class="vc_column-inner ">
                              <div class="wpb_wrapper"></div>
                           </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-8">
                           <div class="vc_column-inner ">
                              <div class="wpb_wrapper">

                                 <div style="" class="heading-title  al-center" id="alian4x_el_5b4d7e7111623">
                                    <h2 style="" class="al-headitg-title al-parallax"><?php the_field('site_title'); ?></h2>
                                    <div  class="al-small-description"><?php the_field('site_sub'); ?></div>
                                    
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-2">
                           <div class="vc_column-inner ">
                              <div class="wpb_wrapper"></div>
                           </div>
                        </div>
                     </div>
                     <div style="" class="al-portfolio-layout">
                        <div id="alian4x_el_5b4d7e7111938" data-size="4" class="al-posts al-masonry-posts">
                           <div class="gutter-sizer"></div>
<?php 
   $site_images = get_field('field_5b78f44c9c90d'); 
   foreach($site_images as $site_items) { ?>
         <article class="<?php 
            $portfolio_image = get_field('field_5b78dfbc37eec');
            foreach($portfolio_image as $portfolio_item) {
               echo str_replace(' ', '', $portfolio_item['work_portfolio_slug']) . ' ';
            }
            ?>">
               <div class="item-wrap dh-container">
                     <div class="post-thumb">
                           <img width="600" height="450" src="<?php echo $site_items['site_images_focus'];?>" class="attachment-kanter_portfolio_popup size-kanter_portfolio_popup wp-post-image" alt="" />
                        </div>
                  </div>
         </article>
<?php   }
?>

                           
                          
                         
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="vc_row-full-width vc_clearfix"></div>
         <section id="contact" style="opacity: 1; padding-bottom: 0!important;" data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true" class="wpb_row vc_row-fluid vc_row-no-padding">
            <div class="worldwide" style="background-image: url(<?php the_field('worldwide_background'); ?>);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h4><?php the_field('worldwide_title'); ?></h4>
                            <p><?php the_field('worldwide_sub'); ?></p>
                            <?php the_field('worldwide_country');?>
                        </div>
                        <div class="col-md-4 text-right">
                            <a href="" class="btn btn-default" style="margin-top: 5em; float: right;">See more</a>
                        </div>
                    </div>
                </div>
            </div>
         </section>
         <script type="text/javascript" src="http://kanter.fidex.com.ua/wp-content/plugins/rev-slider/public/assets/js/extensions/revolution.extension.video.min.js"></script>
         <script type="text/javascript" src="http://kanter.fidex.com.ua/wp-content/plugins/rev-slider/public/assets/js/extensions/revolution.extension.slideanims.min.js"></script>
         <script type="text/javascript" src="http://kanter.fidex.com.ua/wp-content/plugins/rev-slider/public/assets/js/extensions/revolution.extension.actions.min.js"></script>
         <script type="text/javascript" src="http://kanter.fidex.com.ua/wp-content/plugins/rev-slider/public/assets/js/extensions/revolution.extension.layeranimation.min.js"></script>
         <script type="text/javascript" src="http://kanter.fidex.com.ua/wp-content/plugins/rev-slider/public/assets/js/extensions/revolution.extension.navigation.min.js"></script>
         <script type="text/javascript" src="http://kanter.fidex.com.ua/wp-content/plugins/rev-slider/public/assets/js/extensions/revolution.extension.parallax.min.js"></script> 
         
         <div class="vc_row-full-width vc_clearfix"></div>
      </div>
<?php get_footer(); ?>