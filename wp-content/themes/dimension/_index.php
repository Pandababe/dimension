<?php
    /*
    Template Name: Home
    */
get_header() ?>
<body id="top" class="page-template page-template-page-builder page-template-page-builder-php page page-id-2606 wpb-js-composer js-comp-ver-5.4.7 vc_responsive">
      <div class="al-preloader-fixed" id="preloader">
         <div style="background-color: #555" id="status"></div>
      </div>

      <div class="top icon-down toTopFromBottom"><a href="#" class="al-btn-to-top"><i class="pe-7s-angle-up"></i></a></div>
      <div id="home" class="container al-side-container">
         <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
            <div class="wpb_column vc_column_container vc_col-sm-12">
               <div class="vc_column-inner ">
                  <div class="wpb_wrapper">
                     <link href="http://fonts.googleapis.com/css?family=Lato:400%2C500%7CPlayfair+Display:400" rel="stylesheet" property="stylesheet" type="text/css" media="all">
                     <div id="rev_slider_8_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-source="gallery" style="background:#1f1d24;padding:0px; height: 100vh;">
                        <div id="rev_slider_8_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.7.3">
                           <ul>
                              <li data-index="rs-22" data-transition="fadethroughdark" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="2000"  data-thumb=""  data-rotate="0"  data-saveperformance="off"  data-title="Creative" data-param1="01" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                 <img src="images/DSC_0089.jpg"  alt="" title="Home &#8211; Demo 2"  data-bgposition="center center" data-bgfit="cover" data-bgparallax="3" class="rev-slidebg" data-no-retina>
                                 <div class="rs-background-video-layer" 
                                    data-forcerewind="on" 
                                    data-volume="mute" 
                                    data-videowidth="100%" 
                                    data-videoheight="100%" 
                                    data-videomp4="http://dimension.blueinspires.com/video/DIMENSION-ALL.mp4" 
                                    data-videopreload="auto" 
                                    data-videoloop="loop" 
                                    data-aspectratio="16:9" 
                                    data-autoplay="true" 
                                    data-autoplayonlyfirsttime="false" 
                                    ></div>
                                 <div class="tp-caption tp-shape tp-shapewrapper  rs-parallaxlevel-tobggroup" 
                                    id="slide-22-layer-1" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                                    data-fontweight="['100','100','400','400']"
                                    data-width="full"
                                    data-height="full"
                                    data-whitespace="nowrap" 
                                    data-type="shape" 
                                    data-basealign="slide" 
                                    data-responsive_offset="off" 
                                    data-responsive="off"
                                    data-frames='[{"from":"opacity:0;","speed":1500,"to":"o:1;","delay":150,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1500,"to":"opacity:0;","ease":"Power2.easeInOut"}]'
                                    data-textAlign="['left','left','left','left']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="z-index: 5;text-transform:left;background-color:rgba(18,12,20,0.1);"></div>
                                <!--  <div class="tp-caption tp-shape tp-shapewrapper  rs-parallaxlevel-3" 
                                    id="slide-22-layer-4" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-178','-178','-168','-141']" 
                                    data-width="1"
                                    data-height="100"
                                    data-whitespace="nowrap" 
                                    data-type="shape" 
                                    data-responsive_offset="on" 
                                    data-responsive="off"
                                    data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]'
                                    data-textAlign="['left','left','left','left']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="z-index: 6;text-transform:left;background-color:rgba(205,176,131,1);"></div>
                                 <div class="tp-caption Creative-SubTitle   tp-resizeme rs-parallaxlevel-2" 
                                    id="slide-22-layer-3" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-91','-91','-81','-64']" 
                                    data-fontsize="['14','14','14','12']"
                                    data-lineheight="['14','14','14','12']"
                                    data-width="none"
                                    data-height="none"
                                    data-whitespace="nowrap" 
                                    data-type="text" 
                                    data-responsive_offset="on"  data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":2350,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","ease":"Power3.easeInOut"}]'
                                    data-textAlign="['center','center','center','center']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="z-index: 7; white-space: nowrap;text-transform:left;">
                                    Shaping the Future of the Country's</div>
                                 <div class="tp-caption Creative-Title   tp-resizeme rs-parallaxlevel-1" 
                                    id="slide-22-layer-2" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-10','-10','-10','-10']" 
                                    data-fontsize="['70','70','50','40']"
                                    data-lineheight="['70','70','55','45']"
                                    data-width="['none','none','none','320']"
                                    data-height="none"
                                    data-whitespace="nowrap" 
                                    data-type="text" 
                                    data-responsive_offset="on"  data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":2550,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","ease":"Power3.easeInOut"}]'
                                    data-textAlign="['center','center','center','center']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="color: #3ea8ce!important; z-index: 8; white-space: nowrap;text-transform:left;">Concrete <span style="font-size: 30px; display: block; line-height:1;">&amp;</span>  Infrastructure</div>
                                 <div class="tp-caption tp-shape tp-shapewrapper  rs-parallaxlevel-3" 
                                    id="slide-22-layer-5" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['137','137','119','100']" 
                                    data-width="1"
                                    data-height="100"
                                    data-whitespace="nowrap" 
                                    data-type="shape" 
                                    data-responsive_offset="on" 
                                    data-responsive="off"
                                    data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2900,"ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]'
                                    data-textAlign="['left','left','left','left']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="z-index: 9;text-transform:left;background-color:rgba(205,176,131,1);"></div> -->
                                 <div class="tp-caption Creative-Button rev-btn  rs-parallaxlevel-15" 
                                    id="slide-22-layer-6" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['top','top','top','top']" data-voffset="['694','611','689','540']" 
                                    data-fontweight="['400','500','500','500']"
                                    data-width="none"
                                    data-height="none"
                                    data-whitespace="nowrap" 
                                    data-type="button" 
                                    data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]'
                                    data-responsive_offset="on" 
                                    data-responsive="off"
                                    data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":3850,"ease":"Power2.easeOut"},{"delay":"wait","speed":500,"to":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;","ease":"Power1.easeIn"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(205,176,131,1);bc:rgba(205,176,131,1);bw:1px 1px 1px 1px;"}]'
                                    data-textAlign="['left','left','left','left']"
                                    data-paddingtop="[15,15,15,15]"
                                    data-paddingright="[50,50,50,50]"
                                    data-paddingbottom="[15,15,15,15]"
                                    data-paddingleft="[50,50,50,50]"  style="color: #000!important; background-color: rgba(255,255,255,0.5)!important; z-index: 10; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">CONTINUE THE JOURNEY</div>
                              </li>
                              <li data-index="rs-23" data-transition="fadethroughdark" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="2000"  data-thumb="http://kanter.fidex.com.ua/wp-content/uploads/revslider/creativefreedom/creative_bg1-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Quality" data-param1="02" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                 <img src="images/DSC_0109.jpg"  alt="" title="Home &#8211; Demo 2"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="3" class="rev-slidebg" data-no-retina>
                                 <div class="tp-caption tp-shape tp-shapewrapper  rs-parallaxlevel-tobggroup" 
                                    id="slide-23-layer-1" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                                    data-width="full"
                                    data-height="full"
                                    data-whitespace="nowrap" 
                                    data-type="shape" 
                                    data-basealign="slide" 
                                    data-responsive_offset="off" 
                                    data-responsive="off"
                                    data-frames='[{"from":"opacity:0;","speed":1500,"to":"o:1;","delay":150,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1500,"to":"opacity:0;","ease":"Power2.easeInOut"}]'
                                    data-textAlign="['left','left','left','left']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="z-index: 11;text-transform:left;background-color:rgba(18,12,20,0.1);"></div>
                                 <div class="tp-caption tp-shape tp-shapewrapper  rs-parallaxlevel-3" 
                                    id="slide-23-layer-4" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-178','-178','-168','-141']" 
                                    data-width="1"
                                    data-height="100"
                                    data-whitespace="nowrap" 
                                    data-type="shape" 
                                    data-responsive_offset="on" 
                                    data-responsive="off"
                                    data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]'
                                    data-textAlign="['left','left','left','left']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="z-index: 12;text-transform:left;background-color:rgba(205,176,131,1);"></div>
                                 <div class="tp-caption Creative-SubTitle   tp-resizeme rs-parallaxlevel-2" 
                                    id="slide-23-layer-3" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-91','-91','-81','-64']" 
                                    data-fontsize="['14','14','14','12']"
                                    data-lineheight="['14','14','14','12']"
                                    data-width="none"
                                    data-height="none"
                                    data-whitespace="nowrap" 
                                    data-type="text" 
                                    data-responsive_offset="on"  data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":2350,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","ease":"Power3.easeInOut"}]'
                                    data-textAlign="['center','center','center','center']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="z-index: 13; white-space: nowrap;text-transform:left;">Shaping the Future of the Country's</div>
                                 <div class="tp-caption Creative-Title   tp-resizeme rs-parallaxlevel-1" 
                                    id="slide-23-layer-2" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-10','-10','-10','-10']" 
                                    data-fontsize="['70','70','50','40']"
                                    data-lineheight="['70','70','55','45']"
                                    data-width="['none','none','none','320']"
                                    data-height="none"
                                    data-whitespace="nowrap" 
                                    data-type="text" 
                                    data-responsive_offset="on"  data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":2550,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","ease":"Power3.easeInOut"}]'
                                    data-textAlign="['center','center','center','center']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="color: #3ea8ce!important; z-index: 14; white-space: nowrap;text-transform:left;">Concrete <span style="font-size: 30px; display: block; line-height:1;">&amp;</span>  Infrastructure</div>
                                 <div class="tp-caption tp-shape tp-shapewrapper  rs-parallaxlevel-3" 
                                    id="slide-23-layer-5" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['137','137','119','100']" 
                                    data-width="1"
                                    data-height="100"
                                    data-whitespace="nowrap" 
                                    data-type="shape" 
                                    data-responsive_offset="on" 
                                    data-responsive="off"
                                    data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2900,"ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]'
                                    data-textAlign="['left','left','left','left']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="z-index: 15;text-transform:left;background-color:rgba(205,176,131,1);"></div>
                                 <div class="tp-caption Creative-Button rev-btn  rs-parallaxlevel-15" 
                                    id="slide-23-layer-6" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['top','top','top','top']" data-voffset="['694','611','689','540']" 
                                    data-fontweight="['400','500','500','500']"
                                    data-width="none"
                                    data-height="none"
                                    data-whitespace="nowrap" 
                                    data-type="button" 
                                    data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]'
                                    data-responsive_offset="on" 
                                    data-responsive="off"
                                    data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":3850,"ease":"Power2.easeOut"},{"delay":"wait","speed":500,"to":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;","ease":"Power1.easeIn"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(205,176,131,1);bc:rgba(205,176,131,1);bw:1px 1px 1px 1px;"}]'
                                    data-textAlign="['left','left','left','left']"
                                    data-paddingtop="[15,15,15,15]"
                                    data-paddingright="[50,50,50,50]"
                                    data-paddingbottom="[15,15,15,15]"
                                    data-paddingleft="[50,50,50,50]"    style="color: #000!important; background-color: rgba(255,255,255,0.5)!important; z-index: 10; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">CONTINUE THE JOURNEY</div>
                              </li>
                              <li data-index="rs-24" data-transition="fadethroughdark" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="2000"  data-thumb="http://kanter.fidex.com.ua/wp-content/uploads/revslider/creativefreedom/icebg-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Concept" data-param1="03" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                 <img src="images/DSC_0057-3.jpg"  alt="" title="Home &#8211; Demo 2"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="3" class="rev-slidebg" data-no-retina>
                                 <div class="tp-caption tp-shape tp-shapewrapper  rs-parallaxlevel-tobggroup" 
                                    id="slide-24-layer-1" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                                    data-width="full"
                                    data-height="full"
                                    data-whitespace="nowrap" 
                                    data-type="shape" 
                                    data-basealign="slide" 
                                    data-responsive_offset="off" 
                                    data-responsive="off"
                                    data-frames='[{"from":"opacity:0;","speed":1500,"to":"o:1;","delay":150,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1500,"to":"opacity:0;","ease":"Power2.easeInOut"}]'
                                    data-textAlign="['left','left','left','left']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="z-index: 17;text-transform:left;background-color:rgba(18,12,20,0.1);"></div>
                                 <div class="tp-caption tp-shape tp-shapewrapper  rs-parallaxlevel-3" 
                                    id="slide-24-layer-4" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-178','-178','-168','-141']" 
                                    data-width="1"
                                    data-height="100"
                                    data-whitespace="nowrap" 
                                    data-type="shape" 
                                    data-responsive_offset="on" 
                                    data-responsive="off"
                                    data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]'
                                    data-textAlign="['left','left','left','left']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="z-index: 18;text-transform:left;background-color:rgba(205,176,131,1);"></div>
                                 <div class="tp-caption Creative-SubTitle   tp-resizeme rs-parallaxlevel-2" 
                                    id="slide-24-layer-3" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-91','-91','-81','-64']" 
                                    data-fontsize="['14','14','14','12']"
                                    data-lineheight="['14','14','14','12']"
                                    data-width="none"
                                    data-height="none"
                                    data-whitespace="nowrap" 
                                    data-type="text" 
                                    data-responsive_offset="on"  data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":2350,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","ease":"Power3.easeInOut"}]'
                                    data-textAlign="['center','center','center','center']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="z-index: 19; white-space: nowrap;text-transform:left;">Shaping the Future of the Country's</div>
                                 <div class="tp-caption Creative-Title   tp-resizeme rs-parallaxlevel-1" 
                                    id="slide-24-layer-2" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-10','-10','-10','-10']" 
                                    data-fontsize="['70','70','50','40']"
                                    data-lineheight="['70','70','55','45']"
                                    data-width="['none','none','none','320']"
                                    data-height="none"
                                    data-whitespace="nowrap" 
                                    data-type="text" 
                                    data-responsive_offset="on"  data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":2550,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","ease":"Power3.easeInOut"}]'
                                    data-textAlign="['center','center','center','center']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="color: #3ea8ce!important; z-index: 20; white-space: nowrap;text-transform:left;">Concrete <span style="font-size: 30px; display: block; line-height:1;">&amp;</span>  Infrastructure</div>
                                 <div class="tp-caption tp-shape tp-shapewrapper  rs-parallaxlevel-3" 
                                    id="slide-24-layer-5" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['137','137','119','100']" 
                                    data-width="1"
                                    data-height="100"
                                    data-whitespace="nowrap" 
                                    data-type="shape" 
                                    data-responsive_offset="on" 
                                    data-responsive="off"
                                    data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2900,"ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]'
                                    data-textAlign="['left','left','left','left']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"  style="z-index: 21;text-transform:left;background-color:rgba(205,176,131,1);"></div>
                                 <div class="tp-caption Creative-Button rev-btn  rs-parallaxlevel-15" 
                                    id="slide-24-layer-6" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['top','top','top','top']" data-voffset="['694','611','689','540']" 
                                    data-fontweight="['400','500','500','500']"
                                    data-width="none"
                                    data-height="none"
                                    data-whitespace="nowrap" 
                                    data-type="button" 
                                    data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]'
                                    data-responsive_offset="on" 
                                    data-responsive="off"
                                    data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":3850,"ease":"Power2.easeOut"},{"delay":"wait","speed":500,"to":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;","ease":"Power1.easeIn"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(205,176,131,1);bc:rgba(205,176,131,1);bw:1px 1px 1px 1px;"}]'
                                    data-textAlign="['left','left','left','left']"
                                    data-paddingtop="[15,15,15,15]"
                                    data-paddingright="[50,50,50,50]"
                                    data-paddingbottom="[15,15,15,15]"
                                    data-paddingleft="[50,50,50,50]" style="color: #000!important; background-color: rgba(255,255,255,0.5)!important; z-index: 10; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">CONTINUE THE JOURNEY</div>
                              </li>
                           </ul>
                           <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
                              if(htmlDiv) {
                              	htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                              }else{
                              	var htmlDiv = document.createElement("div");
                              	htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                              	document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                              }
                           </script> 
                           <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                        </div>
                        <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss=".tp-caption.Creative-SubTitle,.Creative-SubTitle{color:rgba(205,176,131,1.00);font-size:14px;line-height:14px;font-weight:400;font-style:normal;font-family:Lato;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0 0 0 0px;letter-spacing:2px}.tp-caption.Creative-Title,.Creative-Title{color:rgba(255,255,255,1.00);font-size:70px;line-height:70px;font-weight:400;font-style:normal;font-family:Playfair Display;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0 0 0 0px}.tp-caption.Creative-Button,.Creative-Button{color:rgba(205,176,131,1.00);font-size:13px;line-height:13px;font-weight:400;font-style:normal;font-family:Lato;text-decoration:none;background-color:rgba(0,0,0,0);border-color:rgba(205,176,131,0.25);border-style:solid;border-width:1px;border-radius:0px 0px 0px 0px;letter-spacing:2px}.tp-caption.Creative-Button:hover,.Creative-Button:hover{color:rgba(205,176,131,1.00);text-decoration:none;background-color:rgba(0,0,0,0);border-color:rgba(205,176,131,1.00);border-style:solid;border-width:1px;border-radius:0px 0px 0px 0px;cursor:pointer}";
                           if(htmlDiv) {
                           	htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                           }else{
                           	var htmlDiv = document.createElement("div");
                           	htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                           	document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                           }
                        </script> <script type="text/javascript">if (setREVStartSize!==undefined) setREVStartSize(
                           {c: '#rev_slider_8_1', responsiveLevels: [1240,1024,778,480], gridwidth: [1240,1024,778,480], gridheight: [868,768,960,720], sliderLayout: 'fullscreen', fullScreenAutoWidth:'off', fullScreenAlignForce:'off', fullScreenOffsetContainer:'', fullScreenOffset:'60px'});
                           		
                           var revapi8,
                           tpj;	
                           (function() {			
                           if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded",onLoad); else onLoad();	
                           function onLoad() {				
                           	if (tpj===undefined) { tpj = jQuery; if("off" == "on") tpj.noConflict();}
                           if(tpj("#rev_slider_8_1").revolution == undefined){
                           	revslider_showDoubleJqueryError("#rev_slider_8_1");
                           }else{
                           	revapi8 = tpj("#rev_slider_8_1").show().revolution({
                           		sliderType:"standard",
                           		jsFileLocation:"http://kanter.fidex.com.ua/wp-content/plugins/rev-slider/public/assets/js/",
                           		sliderLayout:"fullscreen",
                           		dottedOverlay:"none",
                           		delay:9000,
                           		navigation: {
                           			keyboardNavigation:"off",
                           			keyboard_direction: "horizontal",
                           			mouseScrollNavigation:"off",
                           							mouseScrollReverse:"default",
                           			onHoverStop:"off",
                           			touch:{
                           				touchenabled:"on",
                           				touchOnDesktop:"off",
                           				swipe_threshold: 75,
                           				swipe_min_touches: 50,
                           				swipe_direction: "horizontal",
                           				drag_block_vertical: false
                           			}
                           			,
                           			tabs: {
                           				style:"metis",
                           				enable:true,
                           				width:250,
                           				height:40,
                           				min_width:249,
                           				wrapper_padding:0,
                           				wrapper_color:"",
                           				tmp:'<div class="tp-tab-wrapper"><div class="tp-tab-number">{{param1}}</div><div class="tp-tab-divider"></div><div class="tp-tab-title-mask"><div class="tp-tab-title">{{title}}</div></div></div>',
                           				visibleAmount: 5,
                           				hide_onmobile: true,
                           				hide_under:800,
                           				hide_onleave:false,
                           				hide_delay:200,
                           				direction:"vertical",
                           				span:true,
                           				position:"inner",
                           				space:0,
                           				h_align:"left",
                           				v_align:"center",
                           				h_offset:0,
                           				v_offset:0
                           			}
                           		},
                           		responsiveLevels:[1240,1024,778,480],
                           		visibilityLevels:[1240,1024,778,480],
                           		gridwidth:[1240,1024,778,480],
                           		gridheight:[868,768,960,720],
                           		lazyType:"none",
                           		parallax: {
                           			type:"3D",
                           			origo:"slidercenter",
                           			speed:1000,
                           			speedbg:0,
                           			speedls:0,
                           			levels:[2,4,6,8,10,12,14,16,45,50,47,48,49,50,0,50],
                           			ddd_shadow:"off",
                           			ddd_bgfreeze:"on",
                           			ddd_overflow:"hidden",
                           			ddd_layer_overflow:"visible",
                           			ddd_z_correction:100,
                           		},
                           		spinner:"off",
                           		stopLoop:"on",
                           		stopAfterLoops:0,
                           		stopAtSlide:1,
                           		shuffle:"off",
                           		autoHeight:"off",
                           		fullScreenAutoWidth:"off",
                           		fullScreenAlignForce:"off",
                           		fullScreenOffsetContainer: "",
                           		fullScreenOffset: "60px",
                           		disableProgressBar:"on",
                           		hideThumbsOnMobile:"off",
                           		hideSliderAtLimit:0,
                           		hideCaptionAtLimit:0,
                           		hideAllCaptionAtLilmit:0,
                           		debugMode:false,
                           		fallbacks: {
                           			simplifyAll:"off",
                           			nextSlideOnWindowFocus:"off",
                           			disableFocusListener:false,
                           		}
                           	});
                           }; /* END OF revapi call */
                           
                           }; /* END OF ON LOAD FUNCTION */
                           }()); /* END OF WRAPPING FUNCTION */
                        </script> <script>var htmlDivCss = unescape("%23rev_slider_8_1%20.metis%20.tp-tab-number%20%7B%0A%20%20%20%20color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%20%20%20%20font-size%3A40px%3B%0A%20%20%20%20line-height%3A30px%3B%0A%20%20%20%20font-weight%3A400%3B%0A%20%20%20%20font-family%3A%20%22Playfair%20Display%22%3B%0A%20%20%20%20width%3A%2080px%3B%20%20%20%0A%20%20%20%20display%3A%20inline-block%3B%0A%09position%3Aabsolute%3B%0A%20%20%20%20text-align%3Acenter%3B%0A%20%20%20%20box-sizing%3Aborder-box%3B%0A%7D%0A%0A%0A%23rev_slider_8_1%20.metis%20.tp-tab-mask%20%7B%0A%20%20%20left%3A0px%3B%0A%20%20%20top%3A0px%3B%0A%20%20%20max-width%3A80%20%21important%3B%20%20%20%0A%20%20%20line-height%3A30px%3B%0A%20%20%20transition%3A0.4s%20padding-left%2C%200.4s%20left%2C%200.4s%20max-width%3B%0A%7D%0A%0A%23rev_slider_8_1%20.metis%3Ahover%20.tp-tab-mask%7B%0A%20%20%20left%3A15px%3B%0A%20%20%20padding-left%3A0px%3B%0A%20%20%20max-width%3A500px%20%21important%3B%0A%7D%0A%0A%23rev_slider_8_1%20.metis%20.tp-tab-divider%20%7B%20%0A%09border-right%3A%201px%20solid%20transparent%3B%0A%20%20%20%20height%3A%2030px%3B%0A%20%20%20%20width%3A%201px%3B%0A%20%20%20%20display%3A%20inline-block%3B%0A%20%20%20%20position%3Aabsolute%3B%0A%20%20%20%20left%3A80px%3B%0A%20%20%20%20transition%3A0.4s%20all%3B%0A%7D%0A%0A%23rev_slider_8_1%20.metis%20.tp-tab-title%20%7B%0A%20%20%20%20color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%20%20%20%20font-size%3A20px%3B%0A%20%20%20%20line-height%3A20px%3B%0A%20%20%20%20font-weight%3A400%3B%0A%20%20%20%20font-family%3A%20%22Playfair%20Display%22%3B%0A%20%20%20%20position%3Arelative%3B%0A%20%20%20%20line-height%3A30px%3B%0A%20%20%20%20padding-left%3A%2030px%3B%0A%20%20%20%20display%3A%20inline-block%3B%0A%20%20%20%20transform%3Atranslatex%28-100%25%29%3B%0A%20%20%20%20transition%3A0.4s%20all%3B%0A%7D%0A%0A%23rev_slider_8_1%20.metis%20.tp-tab-title-mask%20%7B%0A%20%20%20position%3Aabsolute%3B%0A%20%20%20overflow%3Ahidden%3B%0A%20%20%20left%3A80px%3B%20%0A%7D%0A%0A%23rev_slider_8_1%20.metis%3Ahover%20.tp-tab-title%20%7B%0A%20%20%20transform%3Atranslatex%280%29%3B%0A%20%7D%0A%0A%23rev_slider_8_1%20.metis%20.tp-tab%20%7B%20%0A%09opacity%3A%200.15%3B%0A%20%20%20%20transition%3A0.4s%20all%3B%0A%7D%0A%0A%23rev_slider_8_1%20.metis%20.tp-tab%3Ahover%2C%0A%23rev_slider_8_1%20.metis%20.tp-tab.selected%20%7B%0A%20%20%20%20opacity%3A%201%3B%20%0A%7D%0A%0A%23rev_slider_8_1%20.metis%20.tp-tab.selected%20.tp-tab-divider%20%7B%0A%09border-right%3A%201px%20solid%20%23cdb083%3B%0A%7D%0A%0A%23rev_slider_8_1%20.metis%3Ahover%20.tp-tab-divider%20%7B%0A%09margin-left%3A15px%3B%0A%7D%0A%0A%23rev_slider_8_1%20.metis.tp-tabs%20%7B%0A%20%20%20max-width%3A80px%20%21important%3B%20%20%0A%7D%0A%20%20%0A%23rev_slider_8_1%20.metis.tp-tabs%3Abefore%20%7B%0A%20content%3A%22%20%22%3B%0A%20height%3A100%25%3B%0A%20width%3A80px%3B%20%0A%20border-right%3A1px%20solid%20rgba%28255%2C255%2C255%2C0.10%29%3B%0A%20left%3A0px%3B%0A%20top%3A0px%3B%0A%20position%3Aabsolute%3B%0A%20transition%3A0.4s%20all%3B%0A%20background%3Argba%280%2C0%2C0%2C0.15%29%3B%0A%20box-sizing%3Acontent-box%20%21important%3B%0A%20padding%3A0px%3B%0A%20%7D%0A%20%0A%20%23rev_slider_8_1%20.metis.tp-tabs%3Ahover%3Abefore%7B%0A%20%20width%3A80px%3B%0A%20%20background%3Argba%280%2C0%2C0%2C0.25%29%3B%0A%20%20padding%3A0px%2015px%3B%0A%20%7D%0A%20%20%20%20%20%0A%20%40media%20%28max-width%3A499px%29%7B%0A%20%23rev_slider_8_1%20.metis.tp-tabs%3Abefore%20%7B%0A%20background%3Argba%280%2C0%2C0%2C0.75%29%3B%0A%20%7D%0A%20%7D%0A%20%0A");
                           var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                           if(htmlDiv) {
                           	htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                           }
                           else{
                           	var htmlDiv = document.createElement('div');
                           	htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                           	document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                           }
                        </script> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
        
         <div class="vc_row-full-width vc_clearfix"></div>
         <section id="about" data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section vc_custom_1525183886901 vc_section-has-fill">
            <div class="vc_row wpb_row vc_row-fluid">
               <div class="wpb_column vc_column_container vc_col-sm-2">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper"></div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-8">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div style="" class="heading-title  al-center" id="alian4x_el_5b4d7e70f0d58">
                           <h2 style="" class="al-headitg-title al-parallax">About <span class="span">us</span></h2>
                           <div  class="al-small-description">We create <span class="span">awesome stuff</span></div>
                           <div  class="al-subtitle">
                               
<p>   Dimension-all began in 1976 as the first supplier of system formworks and scaffoldings to the Philippine market. The company carried Coffral and Staflex equipment and began by supplying such notable projects as the La Mesa and Magat Dams and the Metro Plaza Makati buildings.</p>
<p>The 80's saw the company supplying Alimall Cubao, SM North Edsa, ADB Ortigas, GSIS (main) , and Purefoods Plants Baung Batangas projects. Through the 90's, we continued our relationships with contractors such as MDC, DDTKI, CE Construction, EEI Corp., Monolith Construction DATEM and HILMARC’S
In 2003, we began our partnership with Aluma Systems of Canada and are now their sole distributor for the Philippines. Aluma Systems has a commitment to innovation and excellence that is seen in their Table and Truss offerings for vertical construction and their Aluma Frame system for heavy infrastructure.</p>
<p>In August 2015, Dimension-all Inc. became a subsidiary and the sixth Asian member of the SRG Group of Companies, a Japan based formworks and shoring company listed in the Japan Stock Exchange, with  factories and offices based in Japan, Vietnam, Korea , Myanmar ,Thailand and the Philippines.</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-2">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper"></div>
                  </div>
               </div>
            </div>
            <div class="vc_row wpb_row vc_row-fluid">
               <div class="wpb_column vc_column_container vc_col-sm-3">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div class="item-about">
                           <div class="icon">
                              <i  class="pe-7s-exapnd2"></i>
                              <div class="bg-icon"><i  class="pe-7s-portfolio"></i></div>
                           </div>
                           <div class="content">
                              <h3 ><span class="span"></span>Company Information</span></h3>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-3">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                            <div class="item-about">
                            <div class="icon">
                                <i  class="pe-7s-display1"></i>
                                <div class="bg-icon"><i  class="pe-7s-news-paper"></i></div>
                            </div>
                            <div class="content">
                                <h3 ><span class="span"></span>General Information</span></h3>
                            </div>
                            </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-3">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div class="item-about">
                           <div class="icon">
                              <i  class="pe-7s-cash"></i>
                              <div class="bg-icon"><i  class="pe-7s-light"></i></div>
                           </div>
                           <div class="content">
                              <h3 ><span class="span">Mission and Vision</span></h3>
                              
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-3">
                    <div class="vc_column-inner ">
                       <div class="wpb_wrapper">
                          <div class="item-about">
                             <div class="icon">
                                <i  class="pe-7s-cash"></i>
                                <div class="bg-icon"><i  class="pe-7s-leaf"></i></div>
                             </div>
                             <div class="content">
                                <h3 ><span class="span">QUALITY, ENVIRONMENTAL, SAFETY AND HEALTH POLICY</span></h3>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
            </div>
         </section>
         <div class="vc_row-full-width vc_clearfix"></div>
         <div id="services" data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_custom_1524169024815 vc_row-has-fill vc_row-no-padding">
            <div class="wpb_column vc_column_container vc_col-sm-12">
               <div class="vc_column-inner ">
                  <div class="wpb_wrapper">
                     <div class="vc_row wpb_row vc_inner vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-2">
                           <div class="vc_column-inner ">
                              <div class="wpb_wrapper"></div>
                           </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-8">
                           <div class="vc_column-inner ">
                              <div class="wpb_wrapper">
                                 <div style="" class="heading-title  al-center" id="alian4x_el_5b4d7e7100125">
                                    <h4 style="color:#ffffff;" class="al-headitg-title al-parallax">Our <span class="span">Systems</span></h4>
                                    <span  class="al-line-title"></span>
                                    <div style="color:#ffffff;" class="al-subtitle">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.<br /> Mirum est notare quam littera gothica, quam nunc putamus parum.</div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-2">
                           <div class="vc_column-inner ">
                              <div class="wpb_wrapper"></div>
                           </div>
                        </div>
                     </div>
                     <script type="text/javascript">jQuery(document).ready( function() {
                        jQuery( "#al-tabs-alian4x_el_5b4d7e710041b" ).tabs();
                        
                        jQuery('#alian4x_el_5b4d7e710041b').slick({
                            dots: true,
                            dotsClass: 'dots',
                            appendDots: '#services-dots-alian4x_el_5b4d7e710041b',
                            slidesToScroll: 2,
                            autoplay: true,
                            autoplaySpeed: 8000,
                            infinite: true,
                            arrows: true,
                            prevArrow: jQuery('#services-arrows-alian4x_el_5b4d7e710041b > .wrap-prev'),
                            nextArrow: jQuery('#services-arrows-alian4x_el_5b4d7e710041b > .wrap-next'),
                            slidesToShow: 4,
                            responsive: [
                                {
                                    breakpoint: 1170,
                                    settings: {
                                        slidesToShow: 4,
                                        slidesToScroll: 2,
                                        infinite: true,
                                        dots: true
                                    }
                                },
                               {
                                    breakpoint: 1024,
                                    settings: {
                                        slidesToShow: 4,
                                        slidesToScroll: 1,
                                        infinite: false,
                                        dots: true
                                    }
                                },
                                {
                                    breakpoint: 992,
                                    settings: {
                                        slidesToShow: 3,
                                        slidesToScroll: 1,
                                        infinite: false,
                                        dots: true
                                    }
                                },
                        
                                {
                                    breakpoint: 600,
                                    settings: {
                                        slidesToShow: 3,
                                        slidesToScroll: 2
                                    }
                                },
                                {
                                    breakpoint: 500,
                                    settings: {
                                        slidesToShow: 2,
                                        slidesToScroll: 2
                                    }
                                },
                        
                                {
                                    breakpoint: 480,
                                    settings: {
                                        slidesToShow: 1,
                                        slidesToScroll: 1
                                    }
                                }
                            ],
                        });
                        
                        
                        
                        
                        });
                     </script> 
                     <div  class="">
                        <div  id="al-tabs-alian4x_el_5b4d7e710041b" class="tabs al-services-tabs al-dark-content">
                           <div class="al-relative">
                              <div class="container">
                                 <ul id="alian4x_el_5b4d7e710041b" class="services-carousel">
                                    <li class="al-service-head-item">
                                       <a  class="al-no-click" href="#1">
                                          <img src="<?php echo get_template_directory_uri(); ?>/images/systems/icon1.png" class="img-responsive" alt="">
                                          <h4>HK System</h4>
                                    <!--       <p>Excellent for cost & duration reduction due to easy and speedy installation and dismantlement; designed optimum load by minimum parts.</p> -->
                                       </a>
                                    </li>
                                    <li class="al-service-head-item">
                                       <a  class="al-no-click" href="#2">
                                          <img src="<?php echo get_template_directory_uri(); ?>/images/systems/icon2.png" class="img-responsive" alt="">
                                          <h4>Hyper 6T G-Cup</h4>
                                       <!--    <p>Mainly used in civil engineering sites. Each column allows 5.5 tons of loading capacity which can be used in construction sites as support of slab.</p> -->
                                       </a>
                                    </li>
                                    <li class="al-service-head-item">
                                       <a  class="al-no-click" href="#3">
                                          <img src="<?php echo get_template_directory_uri(); ?>/images/systems/icon3.png" class="img-responsive" alt="">
                                          <h4>Table Truss System</h4>
                                         <!--  <p>This systems allows the use of extension staffs at the top and bottom of the table forms, can adapt to most slab and beam configurations.</p> -->
                                       </a>
                                    </li>
                                    <li class="al-service-head-item">
                                       <a  class="al-no-click" href="#4">
                                          <img src="<?php echo get_template_directory_uri(); ?>/images/systems/icon4.png" class="img-responsive" alt="">
                                          <h4>EasyDek System</h4>
                                        <!--   <p>With more than 35 years of experience in providing shoring and formwork, solutions Aluma Systems is pleased to introduce the new modular system Aluma EasyDek.</p> -->
                                       </a>
                                    </li>
                                    <li class="al-service-head-item">
                                       <a  class="al-no-click" href="#5">
                                          <img src="<?php echo get_template_directory_uri(); ?>/images/systems/icon5.png" class="img-responsive" alt="">
                                          <h4>H-Frame & Access Scaffolding</h4>
                                    <!--       <p>We are one of the trusted names of the industry engaged in offering a wide range supreme quality of H-Frame System. These products are designed to cater to the needs of our clients.</p> -->
                                       </a>
                                    </li>
                                    <li class="al-service-head-item">
                                       <a  class="al-no-click" href="#6">
                                          <img src="<?php echo get_template_directory_uri(); ?>/images/systems/icon6.png" class="img-responsive" alt="">
                                          <h4>Aluma Beam</h4>
                                         <!--  <p>An aluminum extrusion with the top hat section carries a timber or plastic insert retained by both serrations in the extrusion and screws. The insert is sufficiently deep to accept screws or nails/.</p> -->
                                       </a>
                                    </li>
                                    <li class="al-service-head-item">
                                       <a  class="al-no-click" href="#7">
                                          <img src="<?php echo get_template_directory_uri(); ?>/images/systems/icon7.png" class="img-responsive" alt="">
                                          <h4>Sure Lock</h4>
                                        <!--   <p>Aluma Sure Lock® is a lightweight modular scaffold system that requires no loose fittings, shortens erection and dismantling time and saves labor costs. Access stair towers are the most common application in the Forming and Shoring industry.</p> -->
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                              <div id="services-arrows-alian4x_el_5b4d7e710041b" class="prev-next-block-rotate">
                                 <div class="wrap-prev">
                                    <div class="prev"><i aria-hidden="true" class="fa fa-angle-left"></i></div>
                                 </div>
                                 <div class="wrap-next">
                                    <div class="next"><i aria-hidden="true" class="fa fa-angle-right"></i></div>
                                 </div>
                              </div>
                           </div>
                           <div id="services-dots-alian4x_el_5b4d7e710041b" class="dots-control-carousel"></div>
                           <div class="animated fadeIn al-content-service-shortcode container" id="1">
                              <div class="col-md-6">
                                 <div class="head-service small-head text-left"></div>
                                 <div class="head-service small-head text-left">
                                    <p>
                                        <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/systems/hksystem.jpg" alt="">
                                    </p>
                                    <p>Excellent for cost & duration reduction due to easy/speedy installation & dismantlement; designed optimum load by minimum parts.</p>
                                 </div>
                                 <div class="al-all-services-link">
                                    <div class="link-full"><a href="http://kanter.fidex.com.ua/service/internet-marketing/" title="Internet marketing">Download Brochure<i aria-hidden="true" class="fa fa-angle-right"></i></a></div>
                                 </div>
                              </div>
                              <div class="col-md-5 col-md-push-1">
                                 <h4>Features</h4>
                                 <ul class="list">
                                        <li>Superb safety of certified products by Korean temporary equipment association</li>
                                        <li>Designed optimum load by minimum material, easy for installation & dismantlement</li>
                                        <li>It has over 80% of Korean domestic market share, superiors compatibility among other leasing companies</li>
                                        <li>Consult different site condition by long-term experience</li>
                                        <li>General material which is proved speed of installation & dismantlement</li>
                                        <li>Prefabricated material which is excellent for cost & duration</li>
                                 </ul>
                              </div>
                           </div>
                           <div class="animated fadeIn al-content-service-shortcode container" id="2">
                              <div class="col-md-6">
                                 <div class="head-service small-head text-left"></div>
                                 <div class="head-service small-head text-left">
                                    <p><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/systems/hyper-parts.jpg" alt=""></p>
                                 </div>
                                 <div class="al-all-services-link">
                                    <div class="link-full"><a href="http://kanter.fidex.com.ua/service/context-advertising/" title="Context advertising">Download Brochure<i aria-hidden="true" class="fa fa-angle-right"></i></a></div>
                                 </div>
                              </div>
                              <div class="col-md-5 col-md-push-1">
                                    <p>Mainly used in civil engineering sites where there are less diversion.</p>
                                    <p>Each Standard (Column) allows 5.5 tons of loading capacity which can be used in construction sites both as support of slab formworks with high floor height and as an access scaffolding.</p>
                              </div>
                            
                           </div>
                           <div class="animated fadeIn al-content-service-shortcode container" id="3">
                              <div class="col-md-6">
                                 <div class="head-service small-head text-left"></div>
                                 <div class="head-service small-head text-left">
                                    <p><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/systems/EasyDek.jpg" alt=""></p>
                                 </div>
                                 <div class="al-all-services-link">
                                    <div class="link-full"><a href="http://kanter.fidex.com.ua/service/app-development/" title="App development">Download Brochure<i aria-hidden="true" class="fa fa-angle-right"></i></a></div>
                                 </div>
                              </div>
                              <div class="col-md-5 col-md-push-1">
                                    <p>Engineered for Fast & Easy One-Time Set Up Typical Shoring Application</p>
                                    <p>With more than 35 years of experience in providing shoring and formwork, solutions Aluma Systems is pleased to introduce the new modular system Aluma EasyDek.</p>
                                    <p>Aluma EasyDek is a modular, panelized decking system that simplifies assembly, shortens production time and saves costs. Aluma EasyDek is easily adapted for use on any kind of building, from highrise towers, to parking garages, to shopping malls, making it one of the most flexible systems available on the market.</p>
                                    <p>The system utilizes all-aluminum Aluma EasySet™ panels, one of the lightest panels in the industry. This makes the Aluma EasyDek system easier to erect and dismantle, improving overall construction time while creating a robust , long-lasting forming surface. The Aluma EasyDek elegant and lightweight modular design makes assembly fast and simple. Its la-bour-friendly components fit pre-cisely into place without use of clamps or wedges, and assembly proficiency can be achieved with minimal training.</p>
                                    <p>The Aluma EasyDek incorporates fully strikeable and lightweight (approx. 20 kg/m2) Aluma EasySet™ panels which makes disassembly and reassembly even faster and simpler. The simple panel to primary beam connection makes it easy to han-dle and erect at the jobsite without the need of skilled labour, cranes, or special tools.</p>
                              </div>
                             
                           </div>
                           <div class="animated fadeIn al-content-service-shortcode container" id="4">
                              <div class="col-md-6">
                                 <div class="head-service small-head text-left"></div>
                                 <div class="head-service small-head text-left">
                                    <p><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/systems/tabletruss-parts.jpg" alt=""></p>                    
                                 </div>
                                 <div class="al-all-services-link">
                                    <div class="link-full"><a href="http://kanter.fidex.com.ua/service/web-design/" title="Web design">Download Brochure<i aria-hidden="true" class="fa fa-angle-right"></i></a></div>
                                 </div>
                              </div>
                              <div class="col-md-5 col-md-push-1">
                                    <p>(Formerly Alumalite Truss System)</p>
                                    <p>The Aluma Flying Form Truss System was developed for the specific purpose of cutting construction cycle times, reducing labor costs and improving productivity on buildings that have repetitive floor plans.</p>
                                    <p>Instead of using the traditional handset frame and cross brace systems, The Aluma truss system enables flying forms to be constructed using the range of versatile truss components together with Aluma Beams ®. These forms are assembled only once and are re-used floor-by-floor, avoiding the need for costly dismantling and re-assembly of the forms as each floor is constructed.</p>
                                    <p>The Aluma Truss System offers substantial savings in set-up and stripping time compared with handset shoring systems. When used with Aluma Beams ® and the range of adjustment and handling accessories, this system gives the outstanding performance needed for today’s high speed construction methods. The Aluma flying Form Truss system can be designed to accommodate most structural configurations.</p>
                              </div>
                             
                           </div>
                           <div class="animated fadeIn al-content-service-shortcode container" id="5">
                              <div class="col-md-6">
                                 <div class="head-service small-head text-left"></div>
                                 <div class="head-service small-head text-left">
                                    <p><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/systems/H-Frame.jpg" alt=""></p>
<p>We are one of the trusted names of the industry engaged in offering a wide range supreme quality of H-Frame System. These products are designed to cater to the needs of our clients.</p>
<p>   A high quality range of Scaffolding Frames-Standard H Frames is being offered to the clients in different specifications. These products have rigid welded frame of horizontal and vertical pipes. Further, these pipes are interconnected by scissor type cross braces by the help of pins and spring clips. Moreover, we can customize these products as per the exact details provided by the esteemed patrons, which has helped us in attaining their maximum level of satisfaction.</p>
<p>   The entire range of products is made available to the clients in a variety of designs, dimensions, finishes and others. These are made using best quality material and sophisticated technology that ensures their long life.</p>
                                 </div>
                                 <div class="al-all-services-link">
                                    <div class="link-full"><a href="http://kanter.fidex.com.ua/service/branding/" title="Branding">Download Brochure<i aria-hidden="true" class="fa fa-angle-right"></i></a></div>
                                 </div>
                              </div>
                              <div class="col-md-5 col-md-push-1">
                                  <h4>Features</h4>
                                 <ul class="list">
                                       <li>User friendly operation </li>
                                       <li>High performance</li>
                                       <li>Low maintenance</li>
                                    <li>Longevity</li>
                                       <li>High strength</li>
                                    <li>Robustness</li>
                                       <li>Rust resistance</li>
                                        <li>High efficiency</li>
                                       <li> Optimal performance</li>
                                       <li> Sturdy Construction</li>
                                 </ul>
                              </div>
                           </div>
                           <div class="animated fadeIn al-content-service-shortcode container" id="6">
                              <div class="col-md-6">
                                 <div class="head-service small-head text-left"></div>
                                 <div class="head-service small-head text-left">
                                    <p><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/systems/Aluma Beam.jpg" alt=""></p>                 
                                 </div>
                                 <div class="al-all-services-link">
                                    <div class="link-full"><a href="http://kanter.fidex.com.ua/service/web-development/" title="Web development">Download Brochure<i aria-hidden="true" class="fa fa-angle-right"></i></a></div>
                                 </div>
                              </div>
                              <div class="col-md-5 col-md-push-1">
                                    <p>   An aluminum extrusion with the top hat section carries a timber or plastic insert retained by both serrations in the extrusion and screws. The insert is sufficiently deep to accept screws or nails, and sufficiently wide to butt plywood overtop. The top flange is thickened to increase stiffness and resistance to impact damage. A continuous bolt slot at the center of bottom flange allows the Aluminum Beam to be clamped to any other Aluminum Beam's component and virtually any other shoring component.</p>
                                    <p>   Standard length of Aluminum 140 Beam starts at 1.20m (4’) and increment of 300mm to a maximum length of 11.70m (38’). Special lengths subject to availability or on a sales basis only.</p>
                              </div>
                           </div>
                                                      <div class="animated fadeIn al-content-service-shortcode container" id="7">
                              <div class="col-md-6">
                                 <div class="head-service small-head text-left"></div>
                                 <div class="head-service small-head text-left">
                                        <p><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/systems/SureLock.jpg" alt=""></p>
                                    </div>
                                 <div class="al-all-services-link">
                                    <div class="link-full"><a href="http://kanter.fidex.com.ua/service/web-development/" title="Web development">Download Brochure<i aria-hidden="true" class="fa fa-angle-right"></i></a></div>
                                 </div>
                              </div>
                              <div class="col-md-5 col-md-push-1">
                                    <p><strong>
                                            SURE LOCK® ACCESS SCAFFOLDING PRE-MEASURED AND PRE-ENGINEERED COMPONENTS MAKE IT EASY TO FIT TOGETHER IN A SAFE CONFIGURATION</strong></p>
                                        <p>Aluma Sure Lock® is a lightweight modular scaffold system that requires no loose fittings, shortens erection and dismantling time and saves labor costs. Access stair towers are the most common application in the Forming and Shoring industry.</p>
                                <h4>Product Advantages</h4>
                                 <ul class="list">
                                     <li>   Components connect together with simple wedge</li>
                                     <li>   8 way rosette, for flexible design</li>
                                     <li>   Engineered to work in various weather conditions </li>
                                     <li>   including cold environments </li>
                                     <li>   Extensive range of award winning accessories</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="vc_row-full-width vc_clearfix"></div>
         <div id="portfolio" data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_custom_1525183328188 vc_row-has-fill vc_row-no-padding">
            <div class="wpb_column vc_column_container vc_col-sm-12">
               <div class="vc_column-inner vc_custom_1520177605244">
                  <div class="wpb_wrapper">
                     <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1520177600677">
                        <div class="wpb_column vc_column_container vc_col-sm-2">
                           <div class="vc_column-inner ">
                              <div class="wpb_wrapper"></div>
                           </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-8">
                           <div class="vc_column-inner ">
                              <div class="wpb_wrapper">
                                 <div style="" class="heading-title  al-center" id="alian4x_el_5b4d7e7111623">
                                    <h2 style="" class="al-headitg-title al-parallax">Site <span class="span">Photos</span></h2>
                                    <div  class="al-small-description">We create <span class="span">awesome stuff</span></div>
                                    
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-2">
                           <div class="vc_column-inner ">
                              <div class="wpb_wrapper"></div>
                           </div>
                        </div>
                     </div>
                     <!-- <div class="al-controls-portfolio center">
                        <ul id="al-control-portfolio" class="text-center">
                           <li id="liact" class="filter select-cat active" data-filter="*">All</li>
                        </ul>
                     </div> -->
                     <div style="" class="al-portfolio-layout">
                        <div id="alian4x_el_5b4d7e7111938" data-size="4" class="al-posts al-masonry-posts">
                           <div class="gutter-sizer"></div>
                           <article class=" folio-apps al-gallery-folio">
                              <a href="">
                                 <div class="item-wrap dh-container">
                                    <div class="post-thumb">
                                        <img width="600" height="450" src="<?php echo get_template_directory_uri(); ?>/images/DSC_0006.jpg" class="attachment-kanter_portfolio_popup size-kanter_portfolio_popup wp-post-image" alt="" /></div>
                                    <div class="content dh-overlay">
                                       <div class="tizer-circle"> <i class="fa fa-sliders"></i></div>
                                       <div class="content-wrap">
                                          <div class="content-va">
                                             <h2>BDO Ortigas</h2>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                </a>
                           </article>
                           <article class=" folio-apps al-gallery-folio">
                                <a href="">
                                   <div class="item-wrap dh-container">
                                      <div class="post-thumb">
                                          <img width="600" height="450" src="<?php echo get_template_directory_uri(); ?>/images/DSC_0011.jpg" class="attachment-kanter_portfolio_popup size-kanter_portfolio_popup wp-post-image" alt="" /></div>
                                      <div class="content dh-overlay">
                                         <div class="tizer-circle"> <i class="fa fa-sliders"></i></div>
                                         <div class="content-wrap">
                                            <div class="content-va">
                                               <h2>BDO Ortigas</h2>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                                  </a>
                             </article>
                             <article class=" folio-apps al-gallery-folio">
                                    <a href="">
                                       <div class="item-wrap dh-container">
                                          <div class="post-thumb">
                                              <img width="600" height="450" src="<?php echo get_template_directory_uri(); ?>/images/DSC_0025.jpg" class="attachment-kanter_portfolio_popup size-kanter_portfolio_popup wp-post-image" alt="" /></div>
                                          <div class="content dh-overlay">
                                             <div class="tizer-circle"> <i class="fa fa-sliders"></i></div>
                                             <div class="content-wrap">
                                                <div class="content-va">
                                                   <h2>BDO Ortigas</h2>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                      </a>
                                 </article>
                                 <article class=" folio-apps al-gallery-folio">
                                        <a href="">
                                           <div class="item-wrap dh-container">
                                              <div class="post-thumb">
                                                  <img width="600" height="450" src="<?php echo get_template_directory_uri(); ?>/images/DSC_0028.jpg" class="attachment-kanter_portfolio_popup size-kanter_portfolio_popup wp-post-image" alt="" /></div>
                                              <div class="content dh-overlay">
                                                 <div class="tizer-circle"> <i class="fa fa-sliders"></i></div>
                                                 <div class="content-wrap">
                                                    <div class="content-va">
                                                       <h2>BDO Ortigas</h2>
                                                    </div>
                                                 </div>
                                              </div>
                                           </div>
                                          </a>
                                     </article>
                                     <article class=" folio-apps al-gallery-folio">
                                            <a href="">
                                               <div class="item-wrap dh-container">
                                                  <div class="post-thumb">
                                                      <img width="600" height="450" src="<?php echo get_template_directory_uri(); ?>/images/DSC_0029.jpg" class="attachment-kanter_portfolio_popup size-kanter_portfolio_popup wp-post-image" alt="" /></div>
                                                  <div class="content dh-overlay">
                                                     <div class="tizer-circle"> <i class="fa fa-sliders"></i></div>
                                                     <div class="content-wrap">
                                                        <div class="content-va">
                                                           <h2>BDO Ortigas</h2>
                                                        </div>
                                                     </div>
                                                  </div>
                                               </div>
                                              </a>
                                         </article>
                                         <article class=" folio-apps al-gallery-folio">
                                                <a href="">
                                                   <div class="item-wrap dh-container">
                                                      <div class="post-thumb">
                                                          <img width="600" height="450" src="<?php echo get_template_directory_uri(); ?>/images/DSC_0032.jpg" class="attachment-kanter_portfolio_popup size-kanter_portfolio_popup wp-post-image" alt="" /></div>
                                                      <div class="content dh-overlay">
                                                         <div class="tizer-circle"> <i class="fa fa-sliders"></i></div>
                                                         <div class="content-wrap">
                                                            <div class="content-va">
                                                               <h2>BDO Ortigas</h2>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                  </a>
                                             </article>
                                             <article class=" folio-apps al-gallery-folio">
                                                    <a href="">
                                                       <div class="item-wrap dh-container">
                                                          <div class="post-thumb">
                                                              <img width="600" height="450" src="<?php echo get_template_directory_uri(); ?>/images/DSC_0048.jpg" class="attachment-kanter_portfolio_popup size-kanter_portfolio_popup wp-post-image" alt="" /></div>
                                                          <div class="content dh-overlay">
                                                             <div class="tizer-circle"> <i class="fa fa-sliders"></i></div>
                                                             <div class="content-wrap">
                                                                <div class="content-va">
                                                                   <h2>BDO Ortigas</h2>
                                                                </div>
                                                             </div>
                                                          </div>
                                                       </div>
                                                      </a>
                                                 </article>
                                                 <article class=" folio-apps al-gallery-folio">
                                                        <a href="">
                                                           <div class="item-wrap dh-container">
                                                              <div class="post-thumb">
                                                                  <img width="600" height="450" src="<?php echo get_template_directory_uri(); ?>/images/DSC_0049.jpg" class="attachment-kanter_portfolio_popup size-kanter_portfolio_popup wp-post-image" alt="" /></div>
                                                              <div class="content dh-overlay">
                                                                 <div class="tizer-circle"> <i class="fa fa-sliders"></i></div>
                                                                 <div class="content-wrap">
                                                                    <div class="content-va">
                                                                       <h2>BDO Ortigas</h2>
                                                                    </div>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                          </a>
                                                     </article>
                         
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="vc_row-full-width vc_clearfix"></div>
         <section data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_section vc_custom_1525188254001 vc_section-has-fill">
            <div class="vc_row wpb_row vc_row-fluid">
               <div class="wpb_column vc_column_container vc_col-sm-12">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div style="" class="heading-title  al-center" id="alian4x_el_5b4d7e71496ee">
                           <h4 style="color:#ffffff;" class="al-headitg-title al-parallax">We are in <span class="span">numbers<span></h4>
                           <span  class="al-line-title"></span>
                           <div style="color:#ffffff;" class="al-subtitle">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.<br /> Mirum est notare quam littera gothica, quam nunc putamus parum</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
               <div class="wpb_column vc_column_container vc_col-sm-3">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div style="" class="al-item-number">
                           <div style="background-color:rgba(66,78,92,0.5)" class="num">
                              <h2   ><span data-min="0" data-max="5" data-delay="1" data-increment="1" class="numscroller">0</span></h2>
                           </div>
                           <div style="background-color:#55606c" class="name">
                              <p  >Completed project</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-3">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div style="" class="al-item-number">
                           <div  class="num">
                              <h2   ><span data-min="0" data-max="10" data-delay="1" data-increment="1" class="numscroller">0</span>+</h2>
                           </div>
                           <div  class="name">
                              <p  >Happy clients</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-3">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div style="" class="al-item-number">
                           <div style="background-color:rgba(66,78,92,0.5)" class="num">
                              <h2   ><span data-min="0" data-max="1" data-delay="1" data-increment="1" class="numscroller">0</span>+</h2>
                           </div>
                           <div style="background-color:#55606c" class="name">
                              <p  >Winning awards</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-3">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div style="" class="al-item-number">
                           <div  class="num">
                              <h2   ><span data-min="0" data-max="8" data-delay="1" data-increment="1" class="numscroller">0</span></h2>
                           </div>
                           <div  class="name">
                              <p  >Years</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
         </section>

         <div class="vc_row-full-width vc_clearfix"></div>
         <section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section vc_custom_1525178803603 vc_section-has-fill">
            <div class="vc_row wpb_row vc_row-fluid">
               <div class="wpb_column vc_column_container vc_col-sm-2">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper"></div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-8">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div style="" class="heading-title  al-center" id="alian4x_el_5b4d7e715efc7">
                           <h2 style="" class="al-headitg-title al-parallax">Completed Projects</h2>
                           <div  class="al-subtitle">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum</div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-2">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper"></div>
                  </div>
               </div>
            </div>
            <script type="text/javascript">jQuery(document).ready( function() {
                jQuery('#alian4x_el_5b4d7e716ddaex').imagesLoaded(function(){
                    jQuery('#alian4x_el_5b4d7e716ddaex').slick({
                        dots: false,
                        speed: 750,
                        dotsClass: 'dots',
                        appendDots: '#partners-dots-alian4x_el_5b4d7e716ddaex',
                        slidesToScroll: 2,
                        autoplay: true,
                        autoplaySpeed: 8000,
                        infinite: true,
                        slidesToShow: 3,
                        prevArrow: jQuery('#partners-arrows-alian4x_el_5b4d7e716ddaex > .wrap-prev'),
                        nextArrow: jQuery('#partners-arrows-alian4x_el_5b4d7e716ddaex > .wrap-next'),
                        responsive: [
                            {
                                breakpoint: 1170,
                                settings: {
                                    slidesToShow: 4,
                                    slidesToScroll: 2,
                                    infinite: false,
                                }
                            },
                            {
                                breakpoint: 1170,
                                settings: {
                                    slidesToShow: 4,
                                    slidesToScroll: 2,
                                    infinite: false,
                                }
                            },
                            {
                                breakpoint: 1024,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 2,
                                    infinite: false,
                                }
                            },
                
                            {
                                breakpoint: 600,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 2
                                }
                            },
                
                            {
                                breakpoint: 480,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                            }
                            // You can unslick at a given breakpoint now by adding:
                            // settings: "unslick"
                            // instead of a settings object
                        ]
                    });
                });
                });
             </script> 
            <div class="vc_row wpb_row vc_row-fluid" id="alian4x_el_5b4d7e716ddaex">
                
               <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-6">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div style="margin:0 0 50px 0;" class="item-team">
                           <div class="img-wrap"> <img src="<?php echo get_template_directory_uri(); ?>/images/projects/1.jpg" alt="" class="img-responsive"></div>
                           <h2 >The West Tower at One Serendra <br>
                              eastern section of Bonifacio Global City
                           </h2>
                           <span >Ayala Land, Inc. (ALI)</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-6">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div style="margin:0 0 50px 0;" class="item-team">
                           <div class="img-wrap"> <img src="<?php echo get_template_directory_uri(); ?>/images/projects/2.jpg" alt="" class="img-responsive"></div>
                           <h2>Shangri-la at the Fort Bonifacio Global City, Taguig, Metro Manila</h2>
                           <span >Shangri-la International Hotel <br>
                                  Management LTD</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-6">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div style="margin:0 0 50px 0;" class="item-team">
                           <div class="img-wrap"> <img src="<?php echo get_template_directory_uri(); ?>/images/projects/3.jpg" alt="" class="img-responsive"></div>
                           <h2 >Globe Telecom Headquarters <br>
Bonifacio Global City, Taguig City</h2>
                           <span>Globe Telecom / Ayala Land Inc.</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-6">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div style="margin:0 0 50px 0;" class="item-team">
                           <div class="img-wrap"> <img src="<?php echo get_template_directory_uri(); ?>/images/projects/4.jpg" alt="" class="img-responsive"></div>
                           <h2 >SM CITY CONSOLACION <br>
Barangay Lamac Consolacion, Cebu</h2>
                           <span >SM Prime Holdings, Inc.</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-6">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div style="margin:0 0 50px 0;" class="item-team">
                           <div class="img-wrap"> <img src="<?php echo get_template_directory_uri(); ?>/images/projects/5.jpg" alt="" class="img-responsive"></div>
                           <h2 >CLIPP Center <br>
Fort Bonifacio, Taguig City</h2>
                           <span>Clipp Machenson Holdings, Inc.</span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="partners-arrows-alian4x_el_5b4d7e716ddaex" class="prev-next-block-rotate">
                    <div class="wrap-prev">
                       <div class="prev"><i aria-hidden="true" class="fa fa-angle-left"></i></div>
                    </div>
                    <div class="wrap-next">
                       <div class="next"><i aria-hidden="true" class="fa fa-angle-right"></i></div>
                    </div>
                 </div>
         </section>
        
         <div class="vc_row-full-width vc_clearfix"></div>
         <section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section vc_custom_1525181890180 vc_section-has-fill">
            <div class="vc_row wpb_row vc_row-fluid">
               <div class="wpb_column vc_column_container vc_col-sm-2">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper"></div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-8">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div style="" class="heading-title  al-center" id="alian4x_el_5b4d7e71676df">
                           <h2 style="" class="al-headitg-title al-parallax">Why choose <span class="span">us ?</span></h2>
                           <div  class="al-small-description">We are create <span class="span">awesome stuff</span></div>
                           <div  class="al-subtitle">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum</div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-2">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper"></div>
                  </div>
               </div>
            </div>
            <div class="vc_row wpb_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-4">
                            <div class="vc_column-inner ">
                               <div class="wpb_wrapper">
                                  <div class="item-about item-choose">
                                     <div class="icon">
                                        <i  class="pe-7s-look"></i>
                                        <div class="bg-icon"><i  class="pe-7s-look"></i></div>
                                     </div>
                                     <div class="content">
                                        <h3 >RETINA <span class="spam">READY</span></h3>
                                        <div class="wpb_text_column wpb_content_element " >
                                           <div class="wpb_wrapper">
                                              <p>Praesent sodales, quam vitae gravida interdum, ex mi bibendum enim, sit amet tristique mi quam vel odio. Donec non nunc condimentum.</p>
                                           </div>
                                        </div>
                                     </div>
                                  </div>
                               </div>
                            </div>
                         </div>
               <div class="wpb_column vc_column_container vc_col-sm-4">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div class="item-about item-choose">
                           <div class="icon">
                              <i  class="pe-7s-medal"></i>
                              <div class="bg-icon"><i  class="pe-7s-medal"></i></div>
                           </div>
                           <div class="content">
                              <h3 >WE'RE <span class="spam">CREATIVE</span></h3>
                              <div class="wpb_text_column wpb_content_element " >
                                 <div class="wpb_wrapper">
                                    <p>Praesent sodales, quam vitae gravida interdum, ex mi bibendum enim, sit amet tristique mi quam vel odio. Donec non nunc condimentum.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-4">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div class="item-about item-choose">
                           <div class="icon">
                              <i  class="pe-7s-wristwatch"></i>
                              <div class="bg-icon"><i  class="pe-7s-wristwatch"></i></div>
                           </div>
                           <div class="content">
                              <h3 >WE'RE <span class="spam">PASSIONATE</span></h3>
                              <div class="wpb_text_column wpb_content_element " >
                                 <div class="wpb_wrapper">
                                    <p>Praesent sodales, quam vitae gravida interdum, ex mi bibendum enim, sit amet tristique mi quam vel odio. Donec non nunc condimentum.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="vc_row wpb_row vc_row-fluid">
               <div class="wpb_column vc_column_container vc_col-sm-4">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div class="item-about item-choose">
                           <div class="icon">
                              <i  class="pe-7s-like"></i>
                              <div class="bg-icon"><i  class="pe-7s-like"></i></div>
                           </div>
                           <div class="content">
                              <h3 >WE LOVE <span class="spam">PEOPLE</span></h3>
                              <div class="wpb_text_column wpb_content_element " >
                                 <div class="wpb_wrapper">
                                    <p>Praesent sodales, quam vitae gravida interdum, ex mi bibendum enim, sit amet tristique mi quam vel odio. Donec non nunc condimentum.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-4">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div class="item-about item-choose">
                           <div class="icon">
                              <i  class="pe-7s-help2"></i>
                              <div class="bg-icon"><i  class="pe-7s-help2"></i></div>
                           </div>
                           <div class="content">
                              <h3 >FRIENDLY <span class="spam">SUPPORT</span></h3>
                              <div class="wpb_text_column wpb_content_element " >
                                 <div class="wpb_wrapper">
                                    <p>Praesent sodales, quam vitae gravida interdum, ex mi bibendum enim, sit amet tristique mi quam vel odio. Donec non nunc condimentum.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-4">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div class="item-about item-choose">
                           <div class="icon">
                              <i  class="pe-7s-phone"></i>
                              <div class="bg-icon"><i  class="pe-7s-phone"></i></div>
                           </div>
                           <div class="content">
                              <h3 >WE'RE <span class="spam">Responsive</span></h3>
                              <div class="wpb_text_column wpb_content_element " >
                                 <div class="wpb_wrapper">
                                    <p>Praesent sodales, quam vitae gravida interdum, ex mi bibendum enim, sit amet tristique mi quam vel odio. Donec non nunc condimentum.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

         </section>
         <div class="vc_row-full-width vc_clearfix"></div>
  
         <div style="" class="heading-title  al-center" id="alian4x_el_5b4d7e7170760">
            <h4 style="" class="al-headitg-title al-parallax">Our Top 5 <span class="span">Customers</span></h4>
            <span  class="al-line-title"></span>
        </div>         
                  
         <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_custom_1525181763344 vc_row-has-fill vc_row-no-padding">
            
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner vc_custom_1525181756566">
                  <div class="wpb_wrapper">
                     <script type="text/javascript">jQuery(document).ready( function() {
                        jQuery('#alian4x_el_5b4d7e716ddae').imagesLoaded(function(){
                            jQuery('#alian4x_el_5b4d7e716ddae').slick({
                                dots: false,
                                speed: 750,
                                dotsClass: 'dots',
                                appendDots: '#partners-dots-alian4x_el_5b4d7e716ddae',
                                slidesToScroll: 2,
                                autoplay: true,
                                autoplaySpeed: 8000,
                                infinite: true,
                                slidesToShow: 3,
                                prevArrow: jQuery('#partners-arrows-alian4x_el_5b4d7e716ddae > .wrap-prev'),
                                nextArrow: jQuery('#partners-arrows-alian4x_el_5b4d7e716ddae > .wrap-next'),
                                responsive: [
                                    {
                                        breakpoint: 1170,
                                        settings: {
                                            slidesToShow: 4,
                                            slidesToScroll: 2,
                                            infinite: false,
                                        }
                                    },
                                    {
                                        breakpoint: 1170,
                                        settings: {
                                            slidesToShow: 4,
                                            slidesToScroll: 2,
                                            infinite: false,
                                        }
                                    },
                                    {
                                        breakpoint: 1024,
                                        settings: {
                                            slidesToShow: 3,
                                            slidesToScroll: 2,
                                            infinite: false,
                                        }
                                    },
                        
                                    {
                                        breakpoint: 600,
                                        settings: {
                                            slidesToShow: 2,
                                            slidesToScroll: 2
                                        }
                                    },
                        
                                    {
                                        breakpoint: 480,
                                        settings: {
                                            slidesToShow: 1,
                                            slidesToScroll: 1
                                        }
                                    }
                                    // You can unslick at a given breakpoint now by adding:
                                    // settings: "unslick"
                                    // instead of a settings object
                                ]
                            });
                        });
                        });
                     </script> 
                    
                     <div style="padding:50px 0 50px 0;background-color:rgba(0,0,0,0.26);" class="al-middle-dots al-partners-carousel">
                        <div class="container" id="alian4x_el_5b4d7e716ddae">
                           <div class="al-partner-item"><img src="<?php echo get_template_directory_uri(); ?>/images/partners/a.png" alt=""></div>
                           <div class="al-partner-item"><img src="<?php echo get_template_directory_uri(); ?>/images/partners/b.png" alt=""></div>
                           <div class="al-partner-item"><img src="<?php echo get_template_directory_uri(); ?>/images/partners/c.jpg" alt=""></div>
                           <div class="al-partner-item"><img src="<?php echo get_template_directory_uri(); ?>/images/partners/d.jpg" alt=""></div>
                           <div class="al-partner-item"><img src="<?php echo get_template_directory_uri(); ?>/images/partners/e.jpg" alt=""></div>
                        </div>
                        <div id="partners-arrows-alian4x_el_5b4d7e716ddae" class="prev-next-block-rotate">
                           <div class="wrap-prev">
                              <div class="prev"><i aria-hidden="true" class="fa fa-angle-left"></i></div>
                           </div>
                           <div class="wrap-next">
                              <div class="next"><i aria-hidden="true" class="fa fa-angle-right"></i></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="vc_row-full-width vc_clearfix"></div>
         <section id="contact" data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
            <div class="vc_row wpb_row vc_row-fluid vc_custom_1520718702539">
               <div class="wpb_column vc_column_container vc_col-sm-2">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper"></div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-8">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div style="" class="heading-title  al-center" id="alian4x_el_5b4d7e7170760">
                           <h4 style="" class="al-headitg-title al-parallax">Contact <span class="span">with us</span></h4>
                           <span  class="al-line-title"></span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-2">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper"></div>
                  </div>
               </div>
            </div>
            <div class="vc_row wpb_row vc_row-fluid">
               <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-1 vc_col-lg-5 vc_col-md-5">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper row">
                   
                        <div class="flip-box-wrap col-md-6" style="margin-bottom:15px;">
                           <div class="flip-box ifb-auto-height  vertical_flip_bottom flip-ifb-auto-height"  style="" >
                              <div class="ifb-flip-box" id="flip-box-wrap-3359">
                                 <div class="ifb-face ifb-front " style="color:#333333;background:#ffffff; border-width: 2px;border-style: solid; border-color:#ededed;">
                                    <div class="ifb-flip-box-section ">
                                       <div class="flip-box-icon">
                                          <div class="ult-just-icon-wrapper  ">
                                             <div class="align-icon" style="text-align:center;">
                                                <div class="aio-icon none "  style="color:#333333;font-size:32px;display:inline-block;"> <i class="icomoon-home"></i></div>
                                             </div>
                                          </div>
                                       </div>
                                       <h4 class="ult-responsive flipbox_heading"  data-ultimate-target='#flip-box-wrap-3359 .ult-responsive.flipbox_heading'  data-responsive-json-new='{"font-size":"","line-height":""}'  style="color:#333333; font-family:&#039;Poppins&#039;;font-weight:300;">Our Address</h4>
                                    </div>
                                 </div>
                                 <div class="ifb-face ifb-back" style="color:#333333;background:#ffffff; border-width: 2px;border-style: solid; border-color:#ededed;">
                                    <div class="ifb-flip-box-section ">
                                       <div class="ifb-flip-box-section-content ult-responsive"  data-ultimate-target='#flip-box-wrap-3359 .ifb-flip-box-section-content.ult-responsive'  data-responsive-json-new='{"font-size":"desktop:14px;","line-height":""}'  style="color:#333333;color:#333333;">
                                          <p style="margin-top: 0;">Unit 906 One Corporate Centre, Meralco Avenue Corner Julia Vargas, San Antonio, Pasig City Philippines 1605</p>
                                          
                                        </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="flip-box-wrap col-md-6" style="margin-bottom:15px;">
                                <div class="flip-box ifb-auto-height  vertical_flip_bottom flip-ifb-auto-height"  style="" >
                                   <div class="ifb-flip-box" id="flip-box-wrap-3359">
                                      <div class="ifb-face ifb-front " style="color:#333333;background:#ffffff; border-width: 2px;border-style: solid; border-color:#ededed;">
                                         <div class="ifb-flip-box-section ">
                                            <div class="flip-box-icon">
                                               <div class="ult-just-icon-wrapper  ">
                                                  <div class="align-icon" style="text-align:center;">
                                                     <div class="aio-icon none "  style="color:#333333;font-size:32px;display:inline-block;"> <i class="icomoon-map-marker"></i></div>
                                                  </div>
                                               </div>
                                            </div>
                                            <h4 class="ult-responsive flipbox_heading"  data-ultimate-target='#flip-box-wrap-3359 .ult-responsive.flipbox_heading'  data-responsive-json-new='{"font-size":"","line-height":""}'  style="color:#333333; font-family:&#039;Poppins&#039;;font-weight:300;">Yard Address Laguna</h4>
                                         </div>
                                      </div>
                                      <div class="ifb-face ifb-back" style="color:#333333;background:#ffffff; border-width: 2px;border-style: solid; border-color:#ededed;">
                                         <div class="ifb-flip-box-section ">
                                            <div class="ifb-flip-box-section-content ult-responsive"  data-ultimate-target='#flip-box-wrap-3359 .ifb-flip-box-section-content.ult-responsive'  data-responsive-json-new='{"font-size":"desktop:14px;","line-height":""}'  style="color:#333333;color:#333333;">
                                               <p style="margin-top: 0;">1293 Km 53 Maharlika Hi-way, Brgy. Milagrosa, Calamba Laguna</p>
                                               
                                             </div>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                             </div>
                             <div class="flip-box-wrap col-md-6" style="margin-bottom:15px;">
                                    <div class="flip-box ifb-auto-height  vertical_flip_bottom flip-ifb-auto-height"  style="" >
                                       <div class="ifb-flip-box" id="flip-box-wrap-3359">
                                          <div class="ifb-face ifb-front " style="color:#333333;background:#ffffff; border-width: 2px;border-style: solid; border-color:#ededed;">
                                             <div class="ifb-flip-box-section ">
                                                <div class="flip-box-icon">
                                                   <div class="ult-just-icon-wrapper  ">
                                                      <div class="align-icon" style="text-align:center;">
                                                         <div class="aio-icon none "  style="color:#333333;font-size:32px;display:inline-block;"> <i class="icomoon-clock"></i></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <h4 class="ult-responsive flipbox_heading"  data-ultimate-target='#flip-box-wrap-3359 .ult-responsive.flipbox_heading'  data-responsive-json-new='{"font-size":"","line-height":""}'  style="color:#333333; font-family:&#039;Poppins&#039;;font-weight:300;">Office Hours</h4>
                                             </div>
                                          </div>
                                          <div class="ifb-face ifb-back" style="color:#333333;background:#ffffff; border-width: 2px;border-style: solid; border-color:#ededed;">
                                             <div class="ifb-flip-box-section ">
                                                <div class="ifb-flip-box-section-content ult-responsive"  data-ultimate-target='#flip-box-wrap-3359 .ifb-flip-box-section-content.ult-responsive'  data-responsive-json-new='{"font-size":"desktop:14px;","line-height":""}'  style="color:#333333;color:#333333;">
                                                   <p style="margin-top: 0;">
                                                        From Mondays to Fridays <br>
                                                        07:30 AM - 05:30 PM
                                                   </p>
                                                   
                                                 </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                        
                        <div class="flip-box-wrap col-md-6" style="margin-bottom:15px;">
                           <div class="flip-box ifb-auto-height  vertical_flip_bottom flip-ifb-auto-height"  style="" >
                              <div class="ifb-flip-box" id="flip-box-wrap-3298">
                                 <div class="ifb-face ifb-front " style="color:#333333;background:#ffffff; border-width: 2px;border-style: solid; border-color:#ededed;">
                                    <div class="ifb-flip-box-section ">
                                       <div class="flip-box-icon">
                                          <div class="ult-just-icon-wrapper  ">
                                             <div class="align-icon" style="text-align:center;">
                                                <div class="aio-icon none "  style="color:#333333;font-size:32px;display:inline-block;"> <i class="icomoon-mail-open-file"></i></div>
                                             </div>
                                          </div>
                                       </div>
                                       <h4 class="ult-responsive flipbox_heading"  data-ultimate-target='#flip-box-wrap-3298 .ult-responsive.flipbox_heading'  data-responsive-json-new='{"font-size":"desktop:15px;","line-height":""}'  style="color:#333333; font-family:&#039;Poppins&#039;;font-weight:300;">Our Email</h4>
                                    </div>
                                 </div>
                                 <div class="ifb-face ifb-back" style="color:#333333;background:#ffffff; border-width: 2px;border-style: solid; border-color:#ededed;">
                                    <div class="ifb-flip-box-section ">
                                       <div class="ifb-flip-box-section-content ult-responsive"  data-ultimate-target='#flip-box-wrap-3298 .ifb-flip-box-section-content.ult-responsive'  data-responsive-json-new='{"font-size":"desktop:14px;","line-height":""}'  style="color:#333333;color:#333333;">
                                          <p style="margin-top: 0;">
                                            Corporate: info@dimension-all.com <br>    
                                            Sales: sales@dimension-all.com
                                          </p>
                                          
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="flip-box-wrap col-md-6" style="margin-bottom:0;">
                                <div class="flip-box ifb-auto-height  vertical_flip_bottom flip-ifb-auto-height"  style="height: 150px;" >
                                   <div class="ifb-flip-box" id="flip-box-wrap-1187">
                                      <div class="ifb-face ifb-front " style="color:#333333;background:#ffffff; border-width: 2px;border-style: solid; border-color:#ededed;">
                                         <div class="ifb-flip-box-section ">
                                            <div class="flip-box-icon">
                                               <div class="ult-just-icon-wrapper  ">
                                                  <div class="align-icon" style="text-align:center;">
                                                     <div class="aio-icon none "  style="color:#333333;font-size:32px;display:inline-block;"> <i class="icomoon-headphones"></i></div>
                                                  </div>
                                               </div>
                                            </div>
                                            <h4 class="ult-responsive flipbox_heading"  data-ultimate-target='#flip-box-wrap-1187 .ult-responsive.flipbox_heading'  data-responsive-json-new='{"font-size":"desktop:15px;","line-height":""}'  style="color:#333333; font-family:&#039;Poppins&#039;;font-weight:300;">Our Phone</h4>
                                         </div>
                                      </div>
                                      <div class="ifb-face ifb-back" style="color:#333333;background:#ffffff; border-width: 2px;border-style: solid; border-color:#ededed;">
                                         <div class="ifb-flip-box-section ">
                                            <div class="ifb-flip-box-section-content ult-responsive"  data-ultimate-target='#flip-box-wrap-1187 .ifb-flip-box-section-content.ult-responsive'  data-responsive-json-new='{"font-size":"desktop:14px;","line-height":""}'  style="color:#333333;color:#333333;">
                                               <p style="margin-top: 0;">
                                                     +63 (2) 997-4001 (Trunkline)   <br>
                                                     Sales Department       local     105 <br>
                                                     Technical Department   local     106 <br>
                                                     Accounting             local     102 <br>
                                                     HR Department          local     103 <br>
                                               </p>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                             </div>
                     </div>
                  </div>
               </div>
               <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-1 vc_col-lg-4 vc_col-md-4">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div role="form" class="wpcf7" id="wpcf7-f1305-p2606-o2" lang="en-US" dir="ltr">
                           <div class="screen-reader-response"></div>
                           <form action="http://kanter.fidex.com.ua/home-demo-2/#wpcf7-f1305-p2606-o2" method="post" class="wpcf7-form" novalidate="novalidate">
                              <div style="display: none;"> <input type="hidden" name="_wpcf7" value="1305" /> <input type="hidden" name="_wpcf7_version" value="5.0.1" /> <input type="hidden" name="_wpcf7_locale" value="en_US" /> <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1305-p2606-o2" /> <input type="hidden" name="_wpcf7_container_post" value="2606" /></div>
                              <div class="al-left contact-form-sub contact-form-white">
                                 <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Your Name *" /></span><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Your Email *" /></span><span class="wpcf7-form-control-wrap your-phone"><input type="number" name="your-phone" value="" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-required wpcf7-validates-as-number" aria-required="true" aria-invalid="false" placeholder="Your Phone" /></span>
                                 <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="3" rows="5" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Your Message *"></textarea></span><input type="submit" value="Send message" class="btn"></p>
                              </div>
                              <div class="wpcf7-response-output wpcf7-display-none"></div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <div class="vc_row-full-width vc_clearfix"></div>
         <section data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_section vc_custom_1520287547218">
            <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
               <div class="wpb_column vc_column_container vc_col-sm-12">
                  <div class="vc_column-inner vc_custom_1520287542149">
                     <div class="wpb_wrapper">
                        <div class="" id="alian4x_el_5b4d7e717416a" data-zoom="14" data-marker="http://kanter.fidex.com.ua/wp-content/uploads/2018/03/map-marker.png" data-height="420px" data-address="One Corporate Centre, Meralco Ave. cor Julia Vargas Ave. San Antonio, Pasig City" data-address-details="Dimension All"></div>
                        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfIvcRb39yOu1OFOX2G0c4jemKpLgpJl0"></script> <script type="text/javascript">jQuery.noConflict()(function($) {
                           jQuery(document).ready(function () {
                               /*-------------------------------------
                                Google maps API
                                -------------------------------------*/
                               if (typeof jQuery.fn.gmap3 !== 'undefined') {
                           
                                   jQuery("#alian4x_el_5b4d7e717416a").each(function() {
                           
                                       var data_zoom = 15,
                                           data_height;
                           
                                       if (jQuery(this).attr("data-zoom") !== undefined) {
                                           data_zoom = parseInt(jQuery(this).attr("data-zoom"),10);
                                       }
                                       if (jQuery(this).attr("data-height") !== undefined) {
                                           data_height = parseInt(jQuery(this).attr("data-height"),10);
                                       }
                           
                                       jQuery(this).gmap3({
                                           marker: {
                                               values: [{
                                                   address: jQuery(this).attr("data-address"),
                                                   data: jQuery(this).attr("data-address-details")
                                               }],
                                               options:{
                                                   draggable: false,
                                                   icon: jQuery(this).attr("data-marker")
                                               },
                                               events:{
                                                   mouseover: function(marker, event, context){
                                                       var map = jQuery(this).gmap3("get"),
                                                           infowindow = jQuery(this).gmap3({get:{name:"infowindow"}});
                                                       if (infowindow){
                                                           infowindow.open(map, marker);
                                                           infowindow.setContent(context.data);
                                                       } else {
                                                           jQuery(this).gmap3({
                                                               infowindow:{
                                                                   anchor:marker,
                                                                   options:{content: context.data}
                                                               }
                                                           });
                                                       }
                                                   },
                                                   mouseout: function(){
                                                       var infowindow = jQuery(this).gmap3({get:{name:"infowindow"}});
                                                       if (infowindow){
                                                           infowindow.close();
                                                       }
                                                   }
                                               }
                                           },
                                           map: {
                                               options: {
                                                   mapTypeId: google.maps.MapTypeId.ROADMAP,
                                                   zoom: data_zoom,
                                                   scrollwheel: false,
                                                   styles: [{"featureType":"administrative","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","elementType":"all","stylers":[{"saturation":-100},{"lightness":"50"},{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"lightness":"30"}]},{"featureType":"road.local","elementType":"all","stylers":[{"lightness":"40"}]},{"featureType":"transit","elementType":"all","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]},{"featureType":"water","elementType":"labels","stylers":[{"lightness":-25},{"saturation":-100}]}]
                                               }
                                           }
                                       });
                                       jQuery(this).css("height", data_height + "px");
                                   });
                           
                               }
                           });
                           });
                        </script><script type="text/javascript" src="http://kanter.fidex.com.ua/wp-content/plugins/rev-slider/public/assets/js/extensions/revolution.extension.video.min.js"></script>
                        <script type="text/javascript" src="http://kanter.fidex.com.ua/wp-content/plugins/rev-slider/public/assets/js/extensions/revolution.extension.slideanims.min.js"></script>
                        <script type="text/javascript" src="http://kanter.fidex.com.ua/wp-content/plugins/rev-slider/public/assets/js/extensions/revolution.extension.actions.min.js"></script>
                        <script type="text/javascript" src="http://kanter.fidex.com.ua/wp-content/plugins/rev-slider/public/assets/js/extensions/revolution.extension.layeranimation.min.js"></script>
                        <script type="text/javascript" src="http://kanter.fidex.com.ua/wp-content/plugins/rev-slider/public/assets/js/extensions/revolution.extension.navigation.min.js"></script>
                        <script type="text/javascript" src="http://kanter.fidex.com.ua/wp-content/plugins/rev-slider/public/assets/js/extensions/revolution.extension.parallax.min.js"></script> 
                     </div>
                  </div>
               </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
         </section>
         <div class="vc_row-full-width vc_clearfix"></div>
      </div>
<?php get_footer(); ?>