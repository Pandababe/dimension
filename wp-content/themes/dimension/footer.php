<footer>
         <div class="container">
            <div class="row">
               <div class="col-md-3">
                  <div id="text-2" class="al-widget widget_text">
                     <div class="textwidget">
                        <p style="margin-top: 4em;"><img class="alignnone size-full wp-image-2875" src="<?php the_field('logo'); ?>" alt="" style="margin-bottom:10px;" /></p>
                        
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div id="nav_menu-3" class="al-widget widget_nav_menu">
                     <h5 class="al-widget-title">Contact Us</h5>
                     <p>
                        <i class="fa fa-building-o" aria-hidden="true"></i>&nbsp;&nbsp;Location: <?php the_field('footer_location'); ?>
                    </p>
                    <br>
                    <p>
                        <i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;Landline: <?php the_field('footer_landline'); ?>
                    </p>
                    <p>        
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp;E-mail: <?php the_field('footer_email'); ?>
                    </p> 
                  </div>
               </div>
               <div class="col-md-3">
                  <div id="recent-posts-2" class="al-widget widget_recent_entries">
                     <h5 class="al-widget-title">Warehouse</h5>
                     <p>
                        <i class="fa fa-location-arrow" aria-hidden="true"></i>&nbsp;&nbsp; <?php the_field('footer_location_main'); ?>
                     </p>
                  </div>
               </div>
               <div class="col-md-3">
                  <div id="meta-3" class="al-widget widget_meta">
                     <h5 class="al-widget-title">Home</h5>
                     <ul class="social">
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Systems</a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="down-footer">
            <div class="container">
               <div class="row">
                  <div class="col-md-12 text-center">
                     <p>Dimension all &copy; 2018 All Rights Reserved.  -   Created by <a href="www.blueinspires.com">BlueInspires</a></p>
                  </div>
               </div>
            </div>
         </div>
      </footer>
<?php wp_footer(); ?>
<script type="text/javascript" defer src="<?php echo get_template_directory_uri() ?>/js/autoptimize_079102534b582c713dec6f27cbdc803e.js"></script>

</body>
</html>