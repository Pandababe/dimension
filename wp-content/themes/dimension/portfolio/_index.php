<?php
/*
	Template Name: Portfolio
*/
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Portfolio | Formworks and Shoring Solutions</title>
	</head>

	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/portfolio/slick/slick.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/portfolio/css/style.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/portfolio/css/pe.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/portfolio/css/hover.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto+Slab%3A400%2C500%2C600%2C700%2C300%7CRoboto%3A400%2C500%2C700%2C300%7CPacifico%7CPoppins%7CLato%3A900&ver=1.0.0">
	<body>
		<main>
			<section class="main-slider">
				<?php
				$current_page_id = get_the_ID();
				$portfolio = get_field('field_5b51425e94639', $current_page_id);
				foreach($portfolio as $item) {
				?>
				<div class="slider-section" style="background-image: url(<?php echo $item['background_image']; ?>">
					<div class="slider-content bg-white">
						<p class="top-heading">Line of System</p>
						<h1 class="heading"><?php echo $item['title']; ?></h1>
						<div class="description"><?php echo $item['description']; ?></div>
						<ul class="items">
							<li>Overview</li>
							<li>Client</li>
							<li>Type</li>
						</ul>
						<p class="brochure hvr-wobble-horizontal"><a href="">Inquire Brochure</a></p>
						<p class="video hvr-wobble-horizontal"><a href="">Watch Video</a></p>
					</div>
				</div>
				<?php 
				}
				?>
			</section>
			<div class="pager">
				<p class="prev hvr-bob"><a href="#"><i class="pe-7s-left-arrow"></i></a></p>
				<p class="next hvr-bob"><a href="#"><i class="pe-7s-right-arrow"></i></a></p>
			</div>

		</main>
		<div class="thb-page-preloader">
			<svg class="material-spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
				<circle class="material-path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
			</svg>
		</div>
		<script src="<?php echo get_template_directory_uri(); ?>/portfolio/js/jquery.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/portfolio/slick/slick.js"></script>
		<script>
			$(window).on('load', function() { 
				$('.thb-page-preloader').css('visibility', 'hidden');
			});
			$('.main-slider').slick({
				draggable: true,
				infinite: false,
				nextArrow: '.next a',
				prevArrow: '.prev a',
				speed: 1000,
				autoplay: true,
				autoplaySpeed: 100000,

			});

		</script>
	</body>
</html>