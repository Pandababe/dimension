<?php
/*
	Template Name: Portfolio
*/
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Portfolio | Formworks and Shoring Solutions</title>
	</head>

	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/portfolio/slick/slick.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/portfolio/css/style.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/portfolio/css/pe.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/portfolio/css/hover.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/autoptimize_fcc2700cc7f335d019d4ef292542b1d8.css">

	<body>
		<main>

			<div class="banner-area" style="background-image: url('<?php echo get_field('p_background'); ?>');">
				<div class="logo-port"><a href="/"><img src="<?php echo get_field('p_logo'); ?>" alt=""></a></div>
				<div class="banner-content">
					<h1>
						<span><?php echo get_field('p_sub_title'); ?></span>
						<?php echo get_field('p_title'); ?>
					</h1><br>
					<p class="request-quote"><a href="/#contact">Request Free Quote</a></p>
				</div>
				<div class="overlay"></div>
			</div>
			<div class="portfolio-navigation clearfix">

				
				<ul>
					<?php $work_list = get_field('field_5b78df5a37eea', 5);
					$count = 0;
					foreach( $work_list as $items ) {
						$count ++;
					?>
					<li><a href="#" class="<?php echo($count == 1)? 'active' :''; ?>" data-slug="<?php echo $items['works_list_title_text']; ?>"><?php echo $items['works_list_title_text']; ?></a></li>
					<?php } ?>
				</ul>
			</div>
			<?php
			$portfolio_content = get_field('field_5bab2e78535a5');
			$count = 0;
			foreach( $portfolio_content as $item ) {
				$count++;
			?>
			<div class="main-content <?php echo ($count == 1)? 'active': ''; ?>" id="<?php echo $item['p_portfolio_content_slug']; ?>">
				<div class="block-01">
					<div class="vc_row" style="margin-bottom: 30px;">
						<div class="wpb_column vc_column_container vc_col-sm-2">
						</div>
						<div class="wpb_column vc_column_container vc_col-sm-10">
							<div class="vc_col-md-4">
                              <h2 class="title"><?php echo $item['p_portfolio_content_title']; ?></h2>
                              <p class="tagline"><?php echo $item['p_portfolio_content_tagline']; ?></p>
							</div>
							<div class="vc_col-md-7 sys-cont">
								<?php echo $item['p_portfolio_content_01']; ?>
								<br><br><br>
								<h3>SYSTEM OVERVIEW </h3>
								<?php echo $item['p_portfolio_content_02']; ?>
							</div>
						</div>
					</div>
                  <div class="vc_row">
                   
                    <div class="wpb_column vc_column_container vc_col-sm-2">
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-10">
                      <div class="vc_col-md-4" style="margin-bottom: 20px;">
                        <p class="download-brochure">
                          <a href="<?php echo $item['download_link']; ?>" download>
                            <img src="<?php echo $item['download_link_image']; ?>" alt="">
                          </a>
                        </p>
                      </div>
                      <div class="vc_col-md-7">
                        <?php echo $item['p_table']; ?>
                      </div>
                    </div>
                  </div>
				</div>

				<div class="block-02">
					<h2 class="title"><?php echo $item['p_portfolio_slider_title']; ?></h2>
					<div class="portfolio-slider">
						<?php
				foreach( $item['p_portfolio_content_images'] as $image ) {
						?>
						<div class="item">
							<img src="<?php echo $image['p_portfolio_image_item']; ?>" alt="">
							<div class="slider-text">
                               <div class="slider-text-bg">
                                 <h3><?php echo $image['p_portfolio_image_item_title']; ?></h3>
                                 <p><?php echo $image['p_portfolio_image_item_text']; ?></p>
                              </div>
							</div>
						</div>
						<?php
				}
						?>
					</div>

				</div>
			</div>
			<?php
			}
			?>
		</main>
		<footer class="footer-content">
			<div class="vc_row">
				<div class="wpb_column vc_column_container vc_col-md-2"></div>
				<div class="wpb_column vc_column_container vc_col-md-4">
					<?php the_field('p_contacts'); ?>
				</div>
				<div class="wpb_column vc_column_container vc_col-md-2">
				  <ul class="sponsor_bot clear">
					<?php
                      $sponsor = get_field('field_5b78d37879e94', 5);
                      foreach( $sponsor as $item) {
                    ?>
                  <li><img src="<?php echo $item['about_sponsor_image']; ?>" alt=""></li>
                  <?php } ?>
                  </ul>
				</div>
				<div class="wpb_column vc_column_container vc_col-md-2">
				    <?php echo get_field('footer_takayama_logo'); ?>
				</div>
			</div>
		</footer>

		<script src="<?php echo get_template_directory_uri(); ?>/portfolio/js/jquery.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/portfolio/slick/slick.js"></script>
		<script>
			var url = window.location.hash;
			$('.portfolio-slider').slick({
				draggable: true,
				infinite: true,
				speed: 1000,
				autoplay: false,
				autoplaySpeed: 5000,
			});

			$('.portfolio-navigation ul li a').click(function(){
				var slug = $(this).data('slug');
				$(this).addClass('active').parent().siblings().find('a').removeClass('active');
				$('#' + slug).addClass('active').siblings().removeClass('active');
              $('.portfolio-slider').slick('setPosition');
				return false;
			});

			$('.portfolio-navigation ul li a').each(function(){
				
				var slug = url.split('#');
				if( $(this).data('slug') == slug[1] ) {
					$(this).addClass('active').parent().siblings().find('a').removeClass('active');
				}
			});
			
			$(url).addClass('active').siblings().removeClass('active');

		</script>
	</body>
</html>